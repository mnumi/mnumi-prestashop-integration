<?php

if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_1_6($object)
{
    $sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product ADD id INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY ( id ) ;';
    return Db::getInstance()->execute($sql);
}
