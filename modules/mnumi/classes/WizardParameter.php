<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * WizardParameter class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class WizardParameter 
{
    /** @var string $action */
    private $action;

    /** @var array $availableActions Available action names */
    private $availableActions = array(
        'initE',
        'editE',
        'initOrder',
        'pdfE/low',
        'previewE',
        'getE',
    );

    /** @var array List of parameters */
    private $parameters = array();

    /** @var SignatureManager $signatureManager */
    public $signatureManager;
    /**
     * @var null
     */
    private $wizardId;

    /**
     * Constructor
     *
     * @param $action Action name
     * @throws Exception
     */
    public function __construct($action)
    {
        $this->setAction($action);

        $secretKey = Configuration::get('MNUMI_WIZARD_KEY');

        if(!$secretKey)
        {
            throw new Exception('Missing secret key');
        }

        $this->signatureManager = new SignatureManager($secretKey);

    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set parameter to request
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    /**
     * Set action for parameter
     *
     * @param string $action
     * @throws Exception
     */
    public function setAction($action)
    {
        if(!in_array($action, $this->availableActions))
        {
            throw new Exception(sprintf('Incorrect action "%s".', $action));
        }

        $this->set('action', $action);

        $this->action = $action;
    }

    /**
     * Get all parameters
     *
     * @return array
     */
    public function getAll()
    {
        return $this->parameters;
    }

    /**
     * Get encoded parameters
     *
     * @return string
     */
    public function getEncoded()
    {
        return $this->signatureManager->encodeParameter($this->getAll());
    }

    /**
     * Get signature of parameters
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signatureManager->generateSignature($this->getAll());
    }

    /**
     * Get encoded parameters and signature
     *
     * @return array
     */
    public function asArray()
    {
        $params = array(
            'parameter' => $this->getEncoded(),
            'signature' => $this->getSignature()
        );

        if($this->wizardId)
        {
            $params['id'] = $this->wizardId;
        }

        return $params;
    }

    /**
     * Render URL
     *
     * @return string
     */
    public function render()
    {
        $host = Configuration::get('MNUMI_WIZARD_URL');
        $uri = $this->getAction();

        $uriParam = '?' . http_build_query($this->asArray());

        return $host.$uri.$uriParam;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
    
    /**
     * Returns prepared array with parameters.
     * 
     * @param string $productName
     * @param string $backUrl
     * @param string $action
     * @param string $orderId
     * @param integer $pageNr
     * @param integer $width
     * @param integer $height
     * @param string $countChange
     * @param integer $count
     * @return array 
     */
    public static function getPreparedArrayParameter($productName = null, 
            $backUrl = '', $action = 'initOrder', $orderId = null,
            $pageNr = null, $width = null, $height = null, $countChange = null,
            $count = null, $wizards = null)
    {
        // mandatory
        $parameterArray = array(
            'action' => $action,
            'backUrl' => $backUrl);
        
        if(isset($productName))
        {
            $parameterArray['productName'] = $productName;
        }
        
        if(isset($orderId))
        {
            $parameterArray['orderId'] = $orderId;
        }
        
        if(isset($pageNr))
        {
            $parameterArray['pageNr'] = $pageNr;
        }
        
        if(isset($width))
        {
            $parameterArray['width'] = $width;
        }
        
        if(isset($height))
        {
            $parameterArray['height'] = $height;
        }
        
        if(isset($countChange))
        {
            $parameterArray['countChange'] = $countChange;
        }
        
        if(isset($count))
        {
            $parameterArray['count'] = $count;
        }
        
        if(isset($wizards))
        {
            $parameterArray['wizards'] = $wizards;
        }
      
        return $parameterArray;
    }
    
    /**
     * @author  Kevin van Zonneveld &<kevin@vanzonneveld.net>
     * @author  Lachlan Donald
     * @author  Takkie
     * @copyright 2008 Kevin van Zonneveld (http://kevin.vanzonneveld.net)
     * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
     * @version   SVN: Release: $Id: explodeTree.inc.php 89 2008-09-05 20:52:48Z kevin $
     * @link      http://kevin.vanzonneveld.net/
     *
     * @param array   $array
     * @param string  $delimiter
     * @param boolean $baseval
     *
     * @return array
     */
    public static function explodeTree($array, $delimiter = '_', $baseval = false)
    {
        if(!is_array($array))
            return false;
        
        $splitRE   = '/' . preg_quote($delimiter, '/') . '/';
        $returnArr = array();
        foreach ($array as $key => $val)
        {
            // Get parent parts and the current leaf
            $parts  = preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
            $leafPart = array_pop($parts);

            // Build parent structure
            // Might be slow for really deep and large structures
            $parentArr = &$returnArr;
            foreach ($parts as $part)
            {
                if (!isset($parentArr[$part]))
                {
                    $parentArr[$part] = array();
                } elseif (!is_array($parentArr[$part]))
                {
                    if ($baseval)
                    {
                        $parentArr[$part] = array('__base_val' => $parentArr[$part]);
                    } else {
                        $parentArr[$part] = array();
                    }
                }
                $parentArr = &$parentArr[$part];
            }

            // Add the final part to the structure
            if (empty($parentArr[$leafPart]))
            {
                $parentArr[$leafPart] = $val;
            } elseif ($baseval && is_array($parentArr[$leafPart]))
            {
                $parentArr[$leafPart]['__base_val'] = $val;
            }
        }
        return $returnArr;
    }
}