<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of MnumiApi
 *
 * @author dominik
 */
class MnumiApi {

    /**
     * @var string 
     */
    protected $new_api_url;

    /**
     * @var string
     */
    protected $new_api_key;

    /**
     * @var RestClient 
     */
    private $user = false;

    /**
     * @var RestClient 
     */
    private $client = false;

    /**
     * @var RestClient 
     */
    private $carrier = false;

    /**
     * @var RestClient
     */
    private $payment = false;

    /**
     * @var RestClient
     */
    private $product_fields = false;

    /**
     * @var RestClient
     */
    private $product = false;

    /**
     * @var RestClient
     */
    private $products = false;

    /**
     * @var RestClient
     */
    private $category = false;
    
    private $calculation = array();
    
    protected $cache;

    /**
     * @param string $url
     * @param string $key
     */
    public function __construct($url, $key, $cache) {
        $this->new_api_url = $url;
        $this->new_api_key = $key;
        phpFastCache::setup("storage","files");
        phpFastCache::setup("path", $cache);

        // simple Caching with:
        $this->cache = phpFastCache();
    }

    /**
     * 
     * @param array $tmpParams
     * @return RestClient
     */
    public function getClient($tmpParams) {
        if (empty($this->client[md5(json_encode($tmpParams))])) {
            $this->client[md5(json_encode($tmpParams))] = RestClient::get($this->new_api_url . 'clients.json', $tmpParams, $this->new_api_key, $this->new_api_key)
                ->getResponse();
        }
        return $this->client[md5(json_encode($tmpParams))];
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function setClient($tmpParams) {
        return RestClient::post($this->new_api_url . 'clients.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json');
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function getUser($tmpParams) {
        if ($this->user === false) {
            $this->user = RestClient::get($this->new_api_url . 'users.json', $tmpParams, $this->new_api_key, $this->new_api_key)
                    ->getResponse();
        }
        return $this->user;
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function setUser($tmpParams) {
        return RestClient::post($this->new_api_url . 'users.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json');
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function getPayments($tmpParams) {
        $hash = hash("sha512", Tools::JsonEncode($tmpParams));
        $payment = $this->cache->get('Payment'.$hash);
        if ($payment === null) {
            $payment = RestClient::get($this->new_api_url . 'payments.json', $tmpParams, $this->new_api_key, $this->new_api_key)
                    ->getResponse();
            $this->cache->set('Payment'.$hash, $payment , 600);
        }
        return $payment;
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function setPayment($tmpParams) {
        return RestClient::post($this->new_api_url . 'payments.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json')
                        ->getResponse();
    }

    /**
     * @param array $tmpParams
     * @return RestClient
     */
    public function getCarrier($tmpParams) {
        $hash = hash("sha512", Tools::JsonEncode($tmpParams));
        $carrier = $this->cache->get('Carrier'.$hash);
        if ($carrier === null) {
            $carrier = RestClient::get($this->new_api_url . 'carriers.json', $tmpParams, $this->new_api_key, $this->new_api_key)
                    ->getResponse();
            $this->cache->set('Carrier'.$hash, $carrier , 600);
        }
        return $carrier;
    }
    
    public function setCarrier($tmpParams)
    {
        return RestClient::post($this->new_api_url . 'carriers.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json')
                                ->getResponse();
    }

    public function setOrder($order) {
        $response = RestClient::post($this->new_api_url . 'orders.json', Tools::JsonEncode($order), $this->new_api_key, $this->new_api_key, 'application/json')
                ->getResponse();
        @file_put_contents(__DIR__ . '/log.txt', $response);
        return $response;
    }
    
    public function getProductFields($product)
    {
        //$product_fields = $this->cache->get('ProductFields_' . $product);
        //if($product_fields === null)
        //{
            $product_fields = RestClient::get($this->new_api_url . 'products/' . $product . '/fields.json', null, $this->new_api_key, $this->new_api_key)
                            ->getResponse();
        //$this->cache->set('ProductFields_' . $product, $product_fields , 600);
        //}
        return $product_fields;
    }
    
    public function getProduct($id)
    {
        $product = $this->cache->get('Product'.$id);
        if($product === null)
        {
            $product = RestClient::get($this->new_api_url . 'products/' . $id . '.json', null, $this->new_api_key, $this->new_api_key)
                            ->getResponse();
            $this->cache->set('Product'.$id, $product , 600);
        }
        return $product;
    }
    
    public function getCalculationClient($product, $client, $tmpParams)
    {
        return RestClient::post($this->new_api_url . 'calculations/' . $product . '/clients/' . $client . '.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json')
                                ->getResponse();
    }
    
    public function getCalculation($product, $tmpParams)
    {
        $hash = $product.'_'.hash("sha512", Tools::JsonEncode($tmpParams));
        
        $calculation = $this->cache->get('Calculation' . $hash);
        if($calculation === null)
        {
            $calculation = RestClient::post($this->new_api_url . 'calculations/' . $product . '.json', Tools::JsonEncode($tmpParams), $this->new_api_key, $this->new_api_key, 'application/json')
                                ->getResponse();
            $this->cache->set('Calculation' . $hash, $calculation , 600);
        }
        return $calculation;
    }
    
    public function getWizard($product)
    {
        $wizard = $this->cache->get('Wizard'.$product);
        if($wizard === null)
        {
            $wizard = RestClient::get($this->new_api_url . 'products/' . $product . '/wizard.json', null, $this->new_api_key, $this->new_api_key)
                                ->getResponse();
            $this->cache->set('Wizard'.$product, $wizard , 600);
        }
        return $wizard;
    }
    
    public function getCategory()
    {
        $category = $this->cache->get('Category');
        if($category === null)
        {
            $category = RestClient::get($this->new_api_url . 'category.json', null, $this->new_api_key, $this->new_api_key)
                        ->getResponse();
            $this->cache->set('Category', $category , 600);
        }
        return $category;
    }
    
    public function getProducts()
    {
        $products = $this->cache->get('Products');
        if($products === null)
        {
            $products = RestClient::get($this->new_api_url . 'products.json', null, $this->new_api_key, $this->new_api_key)
                        ->getResponse();
            $this->cache->set('Products', $products , 600);
        }
        return $products;
    }
    

}
