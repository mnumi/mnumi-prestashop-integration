<?php
/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

function convertCalculationXmlToArray(SimpleXMLElement $xmlObject)
{
    $items = array();

    foreach ($xmlObject->items->children() as $item) {
        $items[] = array(
            'quantity' => $item->quantity->__toString(),
            'summaryPriceNet' => $item->summaryPriceNet->__toString(),
            'summaryPriceGross' => $item->summaryPriceGross->__toString(),
            'current' => (bool)$item->current->__toString(),
        );
    }

    return array(
        'items' => $items,
        'current' => convertNewOrderXmlToArray($xmlObject->current),
    );
}

function convertNewOrderXmlToArray(SimpleXMLElement $xmlObject)
{
    $resultArray = array();

    $resultArray['tradersPrice'] = $xmlObject->tradersPrice->__toString();
    $resultArray['summaryPriceNet'] = $xmlObject->summaryPriceNet->__toString();
    $resultArray['priceTax'] = $xmlObject->priceTax->__toString();
    $resultArray['summaryPriceGross'] = $xmlObject->summaryPriceGross->__toString();
    $resultArray['factor'] = (float)$xmlObject->factor->__toString();
    $resultArray['count'] = array('label' => $xmlObject->count->label->__toString(),
        'value' => $xmlObject->count->value->__toString());
    $resultArray['quantity'] = array('label' => $xmlObject->quantity->label->__toString(),
        'value' => $xmlObject->quantity->value->__toString());
    $resultArray['size'] = array('label' => $xmlObject->size->label->__toString(),
        'fieldLabel' => $xmlObject->size->fieldLabel->__toString());
    $resultArray['sides'] = array('label' => $xmlObject->sides->label->__toString(),
        'fieldLabel' => $xmlObject->sides->fieldLabel->__toString());
    $resultArray['printSizeWidth'] = (float)$xmlObject->printSizeWidth->__toString();
    $resultArray['printSizeHeight'] = (float)$xmlObject->printSizeHeight->__toString();
    $resultArray['printerWidth'] = (float)$xmlObject->printerWidth->__toString();
    $resultArray['printerHeight'] = (float)$xmlObject->printerHeight->__toString();
    $resultArray['printerName'] = $xmlObject->printerName->__toString();
    $resultArray['measureType'] = $xmlObject->measureType->__toString();
    $resultArray['fixedPrice'] = $xmlObject->fixedPrice->__toString();
    $resultArray['pricelistId'] = $xmlObject->pricelistId->__toString();
    $resultArray['printerPageNb'] = $xmlObject->printerPageNb->__toString();
    $resultArray['square_metre'] = $resultArray['printSizeHeight'] / 1000
        * $resultArray['printSizeWidth'] / 1000
        * $resultArray['quantity']['value']
        * $resultArray['count']['value'];

    $resultArray['priceItems']['COUNT']['name'] = 'COUNT';
    $resultArray['priceItems']['COUNT']['label'] = $xmlObject->count->label->__toString();
    $resultArray['priceItems']['COUNT']['price'] = 0;
    $resultArray['priceItems']['COUNT']['fieldLabel'] = $xmlObject->count->value->__toString();

    $resultArray['priceItems']['QUANTITY']['name'] = 'COUNT';
    $resultArray['priceItems']['QUANTITY']['label'] = $xmlObject->quantity->label->__toString();
    $resultArray['priceItems']['QUANTITY']['price'] = 0;
    $resultArray['priceItems']['QUANTITY']['fieldLabel'] = $xmlObject->quantity->value->__toString();

    $resultArray['priceItems']['MATERIAL']['name'] = $xmlObject->priceItems->MATERIAL->name->__toString();
    $resultArray['priceItems']['MATERIAL']['label'] = $xmlObject->priceItems->MATERIAL->label->__toString();
    $resultArray['priceItems']['MATERIAL']['price'] = (float)$xmlObject->priceItems->MATERIAL->price->__toString();
    $resultArray['priceItems']['MATERIAL']['fieldLabel'] = $xmlObject->priceItems->MATERIAL->fieldLabel->__toString();

    $resultArray['priceItems']['PRINT']['name'] = $xmlObject->priceItems->PRINT->name->__toString();
    $resultArray['priceItems']['PRINT']['label'] = $xmlObject->priceItems->PRINT->label->__toString();
    $resultArray['priceItems']['PRINT']['price'] = (float)$xmlObject->priceItems->PRINT->price->__toString();
    $resultArray['priceItems']['PRINT']['fieldLabel'] = $xmlObject->priceItems->PRINT->fieldLabel->__toString();

    $resultArray['priceItems']['SIZE']['name'] = 'SIZE';
    $resultArray['priceItems']['SIZE']['label'] = $xmlObject->size->label->__toString();
    $resultArray['priceItems']['SIZE']['price'] = 0;
    $resultArray['priceItems']['SIZE']['fieldLabel'] = $xmlObject->size->fieldLabel->__toString();

    $resultArray['priceItems']['SIDES']['name'] = 'SIDES';
    $resultArray['priceItems']['SIDES']['label'] = $xmlObject->sides->label->__toString();
    $resultArray['priceItems']['SIDES']['price'] = 0;
    $resultArray['priceItems']['SIDES']['fieldLabel'] = $xmlObject->sides->fieldLabel->__toString();

    $i = 0;

    //if OTHER's exist
    if ($xmlObject->priceItems->OTHER->count() > 0) {
        foreach ($xmlObject->priceItems->OTHER->children() as $rec) {
            $resultArray['priceItems']['OTHER'][$i]['name'] = $rec->name->__toString();
            $resultArray['priceItems']['OTHER'][$i]['label'] = $rec->label->__toString();
            $resultArray['priceItems']['OTHER'][$i]['price'] = (float)$rec->price->__toString();
            $resultArray['priceItems']['OTHER'][$i]['fieldLabel'] = $rec->fieldLabel->__toString();
            $resultArray['priceItems']['OTHER'][$i]['hidden'] = ($rec->hidden) ? $rec->hidden->__toString() : false;

            $i++;
        }
    }
    return $resultArray;
}
