<?php

/*
 * This file is part of the MnumiPrestashopIntegration package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

if (!defined('_PS_VERSION_')) {
    die();
}

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . '/vendor/');

require_once(dirname(__FILE__) . '/calculation.php');
require_once(dirname(__FILE__) . '/vendor/Zend/Rest/Client.php');
require_once(dirname(__FILE__) . '/vendor/phpfastcache/phpfastcache.php');
require_once(dirname(__FILE__) . '/classes/RestClient.php');
require_once(dirname(__FILE__) . '/classes/MnumiApi.php');


if (!class_exists('Mnumi')) {
    define('UNFRIENDLY_ERROR', false);

    /**
     * Class Mnumi
     */
    class Mnumi extends Module
    {
        /**
         * Session for REST API
         * @var bool|null|string
         */
        public $session = null;
        /**
         * @var null
         */
        public $new_api_key = null;
        /**
         * @var null
         */
        public $new_api_url = null;

        /**
         * @var bool|MnumiApi
         */
        protected $mnumiApi = false;

        /**
         * Module constructor
         */
        public function __construct()
        {
            $this->bootstrap = true;

            $this->name = 'mnumi';
            $this->tab = 'administration';
            $this->version = '1.6.8.1';
            $this->author = 'Mnumi';
            $this->need_instance = 0;
            $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

            parent::__construct();
            $this->displayName = $this->l('Mnumi Integration');
            $this->description = $this->l('Integration with Mnumi Restful API2.0');

            $this->confirmUninstall = $this->l('Are you sure you want to delete your details?');

            $this->session = Tools::substr(
                str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 20)),
                0,
                20
            );

            /**
             * Module key from prestashop.com
             */
            $this->module_key = '3a75e487a4fed932510acfc5db2fad58';

            /**
             * Mnumi API2.0 configuration
             */
            $this->new_api_url = Configuration::get('MNUMI_NEW_API_URL');
            $this->new_api_key = Configuration::get('MNUMI_NEW_API_KEY');

            $this->mnumiApi = new MnumiApi($this->new_api_url, $this->new_api_key, dirname(__FILE__) . '/cache');
        }
        
        /**
         * @param $params
         * @return mixed
         */
        public function hookDisplayShoppingCart($params)
        {
            $cart = $params['cart'];
            if ($this->recalculateCart($cart) === true) {
                $cart->getProducts(true);
                $cart->update(true);
                $params['cart'] = $cart;
                $params['products'] = $cart->getProducts();
                Tools::redirect('index.php?controller=order');
            }

            return $params;
        }

        /**
         * @param $params
         * @return mixed
         */
        public function hookDisplayPaymentTop($params)
        {
            /** @var CartCore $cart */
            $cart = $params['cart'];
            if ($this->recalculateCart($cart) === true) {
                $cart->getProducts(true);
                $cart->update(true);
                $params['cart'] = $cart;
                $params['products'] = $cart->getProducts();
            }

            return $params;
        }

        /**
         * Recalculate cart and update price
         *
         * @param Cart $cart
         * @return bool
         */
        private function recalculateCart(Cart $cart)
        {
            $status = false;
            foreach ($cart->getProducts() as $item) {
                if ($item['raw_attributes']) {
                    $product = new Product($item['id_product']);
                    $postArrayTmp = Tools::JsonDecode($item['raw_attributes'], true);

                    $postArray = array();
                    foreach ($postArrayTmp as $key => $value) {
                        $tmpValue = array_values($value);
                        if ($key === "OTHER") {
                            foreach ($value as $k => $v) {
                                $postArray[$key][$k] = array("name" => $key, "value" => $v);
                            }
                        } elseif ($key === "CUSTOM") {
                            foreach ($value as $k => $v) {
                                $postArray['SIZE'][$k] = array("value" => $v);
                            }
                        } else {
                            $postArray[key($value)] = array("name" => key($value), "value" => $tmpValue[0]);
                        }
                    }
                    $client = null;
                    if ($this->context->customer->isLogged()) {

                        $customer = new Customer((int)$this->context->customer->id);
                        $customerInfo = $customer->getAddresses((int)$this->context->language->id);

                        $tmpParams = array(
                            'emailAddress' => $customer->email
                        );
                        $user = $this->mnumiApi->getUser($tmpParams);
                        $postArray['user'] = $user['username'];
                        $tmpParams = array(
                            'city' => $customerInfo[0]['city'],
                            'postcode' => $customerInfo[0]['postcode'],
                            'street' => $customerInfo[0]['address1'] . ' ' . $customerInfo[0]['address2'],
                        );

                        $client = $this->mnumiApi->getClient($tmpParams);
                        if ((empty($client)) && (!empty($user))) {
                            $country = new Country($customerInfo[0]['id_country']);
                            $tmpParams = array(
                                'client' => array(
                                    'fullname' => ($customer->company) ? $customer->company : $customer->firstname . ' ' . $customer->lastname,
                                    'city' => $customerInfo[0]['city'],
                                    'postcode' => $customerInfo[0]['postcode'],
                                    'street' => $customerInfo[0]['address1'] . ' ' . $customerInfo[0]['address2'],
                                    'country' => $country->iso_code,
                                    'creditLimitAmount' => 0,
                                    'users' => array(0 => array('user' => $user['username']))
                                )
                            );
                            $client = $this->mnumiApi->setClient($tmpParams)->getResponse();
                        }
                        $postArray['client'] = $client[0]['fullname'];
                    }
                    $result = $this->mnumiApi->getCalculation($product->mnumi_id, $postArray);

                    if (!isset($result['error']) && $result) {
                        $result['priceTax'] = (Tax::getProductTaxRate(
                                (int)$item['id_product'],
                                $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}
                            ) / 100);
                        $address = new Address($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
                        if ($this->getTax($address) !== false) {
                            $result['priceTax'] = $this->getTax($address);
                        }

                        $summaryPriceGross = $result['summaryPriceNet'] * $result['priceTax'];
                        $summaryPriceGross += $result['summaryPriceNet'];

                        if ($item['price'] != $summaryPriceGross) {
                            $status = Db::getInstance()->execute(
                                'UPDATE `' . _DB_PREFIX_ . 'cart_product`
                                SET `price` = ' . $summaryPriceGross . ',
                                    `price_wt` = ' . $result['summaryPriceNet'] . ',
                                    `date_add` = NOW()
                                WHERE `id_product` = ' . (int)$item['id_product'] . '
                                AND md5(raw_attributes) = "' . md5($item['raw_attributes']) . '"
                                AND `id_cart` = ' . (int)$cart->id . '
                                LIMIT 1'
                            );
                        }
                    }
                }
            }

            return $status;
        }

        /**
         * Cart save hook
         */
        public function hookActionCartSave()
        {
            $this->context->cookie->unsetFamily('wizard_' . (int)Tools::getValue('id_product'));
        }

        /**
         * Order validation hook
         *
         * @param $params
         * @throws Exception
         */
        public function hookActionValidateOrder($params)
        {

            $tmpParams = array();
            /* @var $cart OrderCore */
            $cart = $params['order'];
            $customer = $params['customer'];
            $orderStatus = $params['orderStatus'];

            if ($cart) {
                $deliveryAddress = new Address($cart->id_address_delivery);
                $invoiceAddress = new Address($cart->id_address_invoice);
                $clientStreet = $invoiceAddress->address1;
                if ($invoiceAddress->address2) {
                    $clientStreet .= ' ' . $invoiceAddress->address2;
                }
                $tmpParams = array(
                    'city' => $deliveryAddress->city,
                    'postcode' => $deliveryAddress->postcode,
                    'street' => $clientStreet,
                    'taxId' => $deliveryAddress->vat_number
                );
                $client = $this->mnumiApi->getClient($tmpParams);

                /**
                 * @see PREST-12
                 */
                $rand = null;

                /**
                 * If client with this NIP not found
                 */
                if (isset($client['error'])) {
                    $tmpParamsTmp = array(
                        'city' => $deliveryAddress->city,
                        'postcode' => $deliveryAddress->postcode,
                        'street' => $clientStreet,
                    );
                    /**
                     * Check again without NIP
                     */
                    $clientTmp = $this->mnumiApi->getClient($tmpParamsTmp);

                    /**
                     * If found client without NIP
                     */
                    if (!isset($clientTmp['error'])) {
                        /**
                         * add sufix
                         */
                        $rand = hash('adler32', json_encode($tmpParams), false);
                    }
                }

                if ($client) {
                    $client = $client[0];
                }
                $tmpParams = array(
                    'emailAddress' => $customer->email
                );
                $user = $this->mnumiApi->getUser($tmpParams);
                if (isset($user['error'])) {
                    $tmpParams = array(
                        'firstName' => $customer->firstname,
                        'lastName' => $customer->lastname,
                        'emailAddress' => $customer->email,
                        'username' => $customer->email,
                        'salt' => md5($customer->email . time()),
                        'password' => $customer->passwd
                    );
                    $user = $this->mnumiApi->setUser($tmpParams)->getResponse();
                }
                if ((empty($client)) && (!empty($user))) {
                    $country = new Country($invoiceAddress->id_country);
                    $tmpParams = array(
                        'client' => array(
                            'fullname' => ($customer->company) ? $customer->company : $customer->firstname . ' ' . $customer->lastname,
                            'city' => $invoiceAddress->city,
                            'postcode' => $invoiceAddress->postcode,
                            'street' => $invoiceAddress->address1 . ' ' . $invoiceAddress->address2,
                            'country' => $country->iso_code,
                            'taxId' => $deliveryAddress->vat_number,
                            'creditLimitAmount' => 0,
                            'users' => array(0 => array('user' => $user['username']))
                        )
                    );
                    if ($rand)
                        $tmpParams['client']['fullname'] .= ' (' . $rand . ')';

                    $client = $this->mnumiApi->setClient($tmpParams)->getResponse();
                }

                if ((!empty($client)) && (!empty($user))) {
                    $paymentParams = array('name' => $cart->payment);
                    $payment = $this->mnumiApi->getPayments($paymentParams);
                    if (empty($payment)) {
                        $paymentParams = array(
                            'name' => $cart->payment,
                            'label' => 'Prestashop: ' . $cart->payment,
                        );
                        $payment = $this->mnumiApi->setPayment($paymentParams);
                    }
                    $carrierTmp = new Carrier($cart->id_carrier);

                    $deliveryTime = trim(($carrierTmp->delay[1]) ? $carrierTmp->delay[1] : $carrierTmp->delay[0]);
                    $carrierParams = array(
                        'name' => $carrierTmp->name . ' - ' . $cart->reference,
                        'cost' => $cart->total_shipping_tax_excl,
                        'price' => $cart->total_shipping_tax_excl,
                        'label' => 'Prestashop: ' . $carrierTmp->name,
                        'deliveryTime' => ($deliveryTime) ? $deliveryTime : 30
                    );
                    if ($this->getTax($deliveryAddress) !== false) {
                        $carrierParams['tax'] = $this->getTax($deliveryAddress);
                        $carrierParams['cost'] = $cart->total_shipping_tax_excl;
                        $carrierParams['price'] = $cart->total_shipping_tax_excl;
                    }
                    $carrier = $this->mnumiApi->setCarrier($carrierParams);
                    if (isset($carrier['error'])) {
                        unset($carrierParams['cost']);
                        unset($carrierParams['price']);
                        $carrier = $this->mnumiApi->setCarrier($carrierParams);
                    }
                    if (!$carrier) {
                        throw new Exception('Carrier not found');
                    }
                    $country = new Country($deliveryAddress->id_country);
                    $apiOrder = array(
                        'deliveryName' => ($deliveryAddress->company) ?
                                $deliveryAddress->company :
                                $deliveryAddress->firstname . ' ' . $deliveryAddress->lastname,
                        'deliveryStreet' => $deliveryAddress->address1 . ' ' . $deliveryAddress->address2,
                        'deliveryCity' => $deliveryAddress->city,
                        'deliveryPostcode' => $deliveryAddress->postcode,
                        'deliveryCountry' => $country->iso_code,
                        'invoiceName' => $invoiceAddress->alias,
                        'invoiceStreet' => $invoiceAddress->address1 . ' ' . $invoiceAddress->address2,
                        'invoiceCity' => $invoiceAddress->city,
                        'invoicePostcode' => $invoiceAddress->postcode,
                        'invoiceInfo' => $invoiceAddress->other,
                        'client' => $client['fullname'],
                        'user' => $user['username'],
                        'wantInvoice' => $orderStatus->invoice,
                        'payment' => $payment['name'],
                        'carrier' => $carrier['name'],
                        'paymentStatusName' => ($orderStatus->paid) ? 'paid' : 'waiting',
                        'items' => array(),
                    );
                    $item = array();
                    foreach ($cart->product_list as $product) {
                        $customSize = array();
                        $rawAtributes = array();
                        $price = $product['mnumi_price'];
                        $mnumiAttributes = Tools::JsonDecode($product['raw_attributes'], true);
                        unset($mnumiAttributes['PRODUCT']);
                        if (isset($mnumiAttributes['CUSTOM'])) {
                            $customSize[] = array(
                                'width' => $mnumiAttributes['CUSTOM']['width'],
                                'height' => $mnumiAttributes['CUSTOM']['height']
                            );
                            unset($mnumiAttributes['CUSTOM']);
                        }
                        if ($mnumiAttributes) {
                            foreach ($mnumiAttributes as $key => $value) {
                                if ((string)$key == "OTHER") {
                                    foreach ($value as $k => $v) {
                                        $rawAtributes[(int)$k] = array('field' => $key, 'value' => $v);
                                    }
                                } else {
                                    foreach ($value as $e => $v) {
                                        $rawAtributes[] = array('field' => $e, 'value' => $v);
                                    }
                                }
                            }
                            $productMnumi = DB::getInstance()->getValue(
                                'SELECT mnumi_id FROM ' . _DB_PREFIX_ . 'product WHERE id_product = ' . (int)$product['id_product']
                            );
                            for ($i = 0; $i < $product['quantity']; $i++) {
                                $item[] = array(
                                    'name' => $product['name'],
                                    'product' => $productMnumi,
                                    'shopName' => $this->new_api_key,
                                    'taxValue' => ($this->getTax($deliveryAddress) === false) ? Tax::getProductTaxRate(
                                            $product['id_product']
                                        ) : $this->getTax($deliveryAddress),
                                    'attributes' => $rawAtributes,
                                    'customSize' => (isset($customSize)) ? $customSize : null,
                                );
                            }
                        }
                    }
                    if ($item) {
                        $cartResponse = Tools::JsonEncode(array('ok'));
                        $apiOrder['items'] = $item;
                        $order = array();
                        $order['order'] = $apiOrder;
                        $response = $this->mnumiApi->setOrder($order);
                        if (isset($response['error'])) {
                            $cartResponse = Tools::JsonEncode($response);
                        }
                        /* @var $cookie CookieCore */
                        $cookie = $this->context->cookie;
                        $cookie->unsetFamily('cartResponse_');
                        $cookie->{'cartResponse_' . $params['cart']->id . '_value'} = $cartResponse;
                    }
                }
            }
        }

        /**
         * @param $params
         */
        public function hookOrderConfirmation($params)
        {
            /** @var $cookie CookieCore */
            $cookie = $this->context->cookie;
            /** @var OrderCore $objOrder */
            $objOrder = $params['objOrder'];

            $response = $cookie->{'cartResponse_' . $objOrder->id_cart . '_value'};
            if ($response !== false) {
                /* @var $history OrderHistoryCore */
                $history = new OrderHistory();
                $history->id_order = (int)$objOrder->id;

                /* @var $last OrderStateCore */
                $last = OrderHistory::getLastOrderState($history->id_order);
                $response = Tools::JsonDecode($response, true);
                if (isset($response['error'])) {
                    $mnumiStates = $this->getMnumiState("Wysłanie do API nie powiodło się!");
                    if ($mnumiStates !== false) {
                        $history->changeIdOrderState($mnumiStates, $objOrder->id);
                        $history->save();
                    }
                } else {
                    $mnumiStates = $this->getMnumiState("Wysłano do API");
                    if ($mnumiStates !== false) {
                        $history->changeIdOrderState($mnumiStates, $objOrder->id);
                        $history->save();
                    }
                }
                /* @var $history OrderHistoryCore */
                $history = new OrderHistory();
                $history->id_order = (int)$objOrder->id;
                $history->changeIdOrderState($last->id, $objOrder->id);
                $history->save();
            }
        }

        /**
         * Get mnumi state from OrderState by name
         *
         * @param $name
         * @return bool
         */
        private function getMnumiState($name)
        {
            $r = new OrderState();
            $orderState = $r->getOrderStates($this->context->language->id);
            foreach ($orderState as $state) {
                if ($state['name'] == $name) {
                    return $state['id_order_state'];
                }
            }

            return false;
        }

        /**
         * Get VIES tax
         *
         * @param Address $address
         * @return bool
         */
        private function getTax(Address $address)
        {
            if (class_exists('VATNumberTaxManager')) {
                if (VATNumberTaxManager::isAvailableForThisAddress($address)) {
                    /* @var $tax VATNumberTaxManager */
                    $taxManager = new VATNumberTaxManager();
                    $tax = $taxManager->getTaxCalculator();

                    return $tax->taxes[0]->rate;
                }
            }

            return false;
        }

        /**
         * Header hook
         */
        public function hookHeader()
        {
            if (Configuration::get('PS_CATALOG_MODE'))
                return;

            if ((int)(Configuration::get('PS_BLOCK_CART_AJAX'))) {

                $this->context->controller->removeJS(_THEME_JS_DIR_ . '/modules/blockcart/ajax-cart.js');
                $this->context->controller->addJs($this->_path . 'view/ajax-cart.js');
                $this->context->controller->addCSS($this->_path . 'view/mnumi.css', 'all');
            }
        }

        /**
         * Get Mnumi Field type by name
         *
         * @param $name
         * @return string
         */
        public function getFieldType($name)
        {
            switch ($name) {
                case 'COUNT':
                case 'QUANTITY':
                    return 'text';
                    break;
                case 'SIZE':
                default:
                    return 'select';
                    break;
            }
        }

        /**
         * Prepare & Get inputs from Mnumi
         *
         * @param $mnumi_id
         * @return array|bool
         */
        public function getGroups($mnumi_id)
        {
            $fields = $this->mnumiApi->getProductFields($mnumi_id);

            if (!$fields) {
                return false;
            }
            $types = array(1 => 'text', 4 => 'select', 10 => 'select');
            $groups = array();
            foreach ($fields as $field) {
                if (isset($field['default_value'])) {
                    if ((string)$field['fieldset']['label'] != "Wizard") {
                        $groups[(int)$field['id']] = array(
                            'group_type' => ((bool)$field['visible'] == false) ? 'hidden' : $this->getFieldType(
                                    $field['fieldset']['name']
                                ),
                            'id' => $field['fieldset']['name'],
                            'default' => $field['default_value'],
                            'name' => ($field['label']) ? $field['label'] : $field['fieldset']['label']
                        );
                        if ($this->getFieldType($field['fieldset']['name']) == 'select') {
                            $params = array();
                            foreach ($field['field_items'] as $item) {
                                $params['key_' . $item['field_item']['id']] = array(
                                    'id' => $item['field_item']['id'],
                                    'name' => isset($item['field_item']['label']) ? $item['field_item']['label'] : $item['field_item']['name'],
                                );
                            }
                            $groups[(int)$field['id']]['default'] = intval($field['default_value']);
                            $groups[(int)$field['id']]['name'] = $this -> l((string)($field['label'])?$field['label']:$field['fieldset']['label']);
                            foreach ($params as $attr) {
                                $groups[(int)$field['id']]['attributes'][(int)$attr['id']] = $this->l(
                                    (string)$attr['name']
                                );
                            }
                        }
                    }
                } elseif ((string)$field['fieldset']['label'] != "Wizard") {
                    $groups[(int)$field['id']] = array(
                        'group_type' => 'hidden',
                        'id' => $field['fieldset']['name'],
                        'name' => isset($field['label']) ? $field['label'] : $field['fieldset']['label']
                    );
                } elseif ((string)$field['fieldset']['label'] == "Wizard") {
                    $groups[(int)$field['id']] = array(
                        'group_type' => 'hidden',
                        'id' => $field['fieldset']['name'],
                        'name' => isset($field['label']) ? $field['label'] : $field['fieldset']['label']
                    );
                }
            }

            return $groups;
        }

        /**
         * Prepare Mnumi Params for html
         *
         * @param $params
         * @return mixed
         */
        public function hookMnumiParams($params)
        {
            if ($id_product = (int)Tools::getValue('id_product')) {
                $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
                if ((!Validate:: isLoadedObject($product)) || (empty($product->mnumi_id))) {
                    return;
                }
                $mnumi_id = $product->mnumi_id;
                $quantityBackup = null;
                $groups = $this->getGroups($mnumi_id);
                $groups[] = array(
                    'group_type' => 'hidden',
                    'id' => 'PRODUCT',
                    'default' => $product->mnumi_id,
                    'name' => 'PRODUCT'
                );
                foreach ($groups as $key => &$group) {
                    if ($group['id'] == "FIXEDPRICE" || !isset($group['default'])) {
                        unset($groups[$key]);
                    } else if ($group['id'] == "QUANTITY") {
                        $quantityBackup = $group['default'];
                    } else {
                        $data[$group['id']] = $group['default'];
                        if ($group['id'] == "OTHER") {
                            $group['id'] = $group['id'] . '[' . $key . ']';
                        }
                    }
                }
                $product = $this->mnumiApi->getProduct($mnumi_id);
                if (@$product['is_static_quantity']) {
                    $values = explode(",", $product['static_quantity']);
                    $values = array_combine($values, $values);
                    foreach ($groups as $key => &$group) {
                        if ($group['id'] == "QUANTITY") {
                            $group['attributes'] = $values;
                            $group['static'] = true;
                        }
                    }
                }
                $this->context->smarty->assign(
                    array(
                        'groups' => $groups,
                        'quantityBackup' => $quantityBackup
                    )
                );
                //Context::getContext()->controller->addJs($this->_path . 'view/mnumi.js');
                //$this->context->controller->addJs($this->_path . 'view/mnumi.js');
                return $this->display(__FILE__, 'view/mnumiParams.tpl');
            }
        }

        /**
         * Calculate form Mnumi products
         *
         * @param $array
         * @param $product
         * @param bool $request
         * @return mixed
         */
        public function getCalculation($array, $product, $request = false)
        {
            foreach ($array as $name => $post) {
                if (is_numeric($name)) {
                    $id = $name;
                    $name = $post['name'];
                    $post = $post['value'];
                }

                if (ctype_upper($name)) {
                    if ($name == "OTHER") {
                        if (is_array($post)) {
                            foreach ($post as $key => $value) {
                                if (!isset($id) || $id == null) {
                                    $postArray[$name][$key] = array('name' => $name, 'value' => $value);
                                } else {
                                    $postArray[$name][$id] = array('name' => $name, 'value' => $value);
                                }
                            }

                        } else {
                            if ($id == null) {
                                $id = $name;
                            }
                            $postArray[$name][$id] = array('name' => $name, 'value' => $post);
                        }
                    } else if ($name == "QUANTITY" || $name == "COUNT") {
                        $postArray[$name] = array('name' => $name, 'value' => (int)$post);
                    } else {
                        if (is_array($post)) {
                            $postArray[$name] = $post;
                        } else {
                            $postArray[$name] = array('name' => $name, 'value' => $post);
                        }
                    }
                } else if ($name == "qty") {
                    $postArray['QUANTITY'] = array('name' => 'QUANTITY', 'value' => (int)$post);
                }
            }
            $postArray['NAME'] = array('name' => 'NAME', 'value' => 'targetAction');
            $postArray['ORDER_ID'] = array('name' => 'ORDER_ID', 'value' => 0);
            $postArray['INFORMATIONS'] = array('name' => 'INFORMATIONS', 'value' => null);
            $postArray['VALUE'] = array('name' => 'VALUE', 'value' => 'addNewWizard');
            if (isset($array['size_width']) && isset($array['size_height'])) {
                $postArray['SIZE']['width']['value'] = $array['size_width'];
                $postArray['SIZE']['height']['value'] = $array['size_height'];
                $postArray['SIZE']['metric']['value'] = "mm";
            }
            unset($postArray['FIXEDPRICE']);
            unset($postArray['GENERAL']);
            unset($postArray['PRODUCT']);
            $client = null;
            if ($this->context->customer->isLogged()) {

                $customer = new Customer((int)$this->context->customer->id);
                $customerInfo = $customer->getAddresses((int)$this->context->language->id);

                $tmpParams = array(
                    'emailAddress' => $customer->email
                );
                $user = $this->mnumiApi->getUser($tmpParams);
                $postArray['user'] = $user['username'];
                $tmpParams = array(
                    'city' => $customerInfo[0]['city'],
                    'postcode' => $customerInfo[0]['postcode'],
                    'street' => $customerInfo[0]['address1'] . ' ' . $customerInfo[0]['address2'],
                );

                $client = $this->mnumiApi->getClient($tmpParams);
                if ((empty($client)) && (!empty($user))) {
                    $country = new Country($customerInfo[0]['id_country']);
                    $tmpParams = array(
                        'client' => array(
                            'fullname' => ($customer->company) ? $customer->company : $customer->firstname . ' ' . $customer->lastname,
                            'city' => $customerInfo[0]['city'],
                            'postcode' => $customerInfo[0]['postcode'],
                            'street' => $customerInfo[0]['address1'] . ' ' . $customerInfo[0]['address2'],
                            'country' => $country->iso_code,
                            'creditLimitAmount' => 0,
                            'users' => array(0 => array('user' => $user['username']))
                        )
                    );
                    $client = $this->mnumiApi->setClient($tmpParams)->getResponse();
                }
                $postArray['client'] = $client[0]['fullname'];
            }
            try {
                ksort($postArray);
                $result = $this->mnumiApi->getCalculation($product, $postArray);
                if ($result) {
                    $result['priceTax'] = (Tax::getProductTaxRate((int)Tools::getValue('id_product')) / 100);
                    $address = new Address($this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
                    if ($this->getTax($address) !== false) {
                        $result['priceTax'] = $this->getTax($address);
                    }
                    $result['summaryPriceGross'] = $result['summaryPriceNet'] * $result['priceTax'];
                    $result['summaryPriceGross'] += $result['summaryPriceNet'];

                    return $result;
                }
            } catch (Exception $e) {

            }
        }

        /**
         * Ajax response/request
         *
         * @return string
         */
        public function ajaxCall()
        {
            if (Tools::getValue('method') == "removeWizard") {
                if (($id_product = (int)Tools::getValue('id_product')) && ($wizard = Tools::getValue('wizard'))) {
                    $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
                    if ((!Validate:: isLoadedObject($product)) || (empty($product->mnumi_id))) {
                        return;
                    }
                    $this->context->cookie->unsetFamily('wizard_' . $id_product . '_' . $wizard);

                    return $product->getLink();
                }
            } elseif (Tools::getValue('method') == "addNewWizard") {
                if ($id_product = (int)Tools::getValue('id_product')) {
                    $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
                    if ((!Validate:: isLoadedObject($product)) || (empty($product->mnumi_id))) {
                        return;
                    }
                    require_once(dirname(__FILE__) . '/classes/SignatureManager.php');
                    require_once(dirname(__FILE__) . '/classes/WizardParameter.php');

                    $wizardParameters = new WizardParameter('getE');
                    $url = $wizardParameters->render();

                    $client = new Zend_Rest_Client($url);
                    $result = $client->post();

                    if ($result->isSuccess()) {
                        $wizard = $this->mnumiApi->getWizard($product->mnumi_id);
                        $wizardId = (string)$result->orderId;
                        $wizardParameters = new WizardParameter('initOrder');
                        $wizardParameters
                            ->set('productName', $product->mnumi_id)
                            ->set('backUrl', $product->getLink())
                            ->set('wizards', $wizard[0]['wizard_name'])
                            ->set('orderId', $wizardId);
                        $redirectUrl = $wizardParameters->render();
                        $this->context->cookie->__set('wizard_' . $id_product . '_' . $wizardId . '_value', $wizardId);
                        /* EDIT */
                        $parameterEditDecoded = WizardParameter::getPreparedArrayParameter(
                            $product->mnumi_id,
                            $product->getLink(),
                            'editWizard',
                            $wizardId
                        );

                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_parameterEdit',
                            $wizardParameters->signatureManager->encodeParameter($parameterEditDecoded)
                        );
                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_signatureEdit',
                            $wizardParameters->signatureManager->generateSignature($parameterEditDecoded)
                        );
                        /* END EDIT */

                        /* PDF */
                        $parameterPdfDecoded = WizardParameter::getPreparedArrayParameter(
                            null,
                            null,
                            'pdfE',
                            $wizardId
                        );

                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_parameterPdf',
                            $wizardParameters->signatureManager->encodeParameter($parameterPdfDecoded)
                        );
                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_signaturePdf',
                            $wizardParameters->signatureManager->generateSignature($parameterPdfDecoded)
                        );
                        /* END PDF */

                        /* PREVIEW */
                        $parameterDecoded = WizardParameter::getPreparedArrayParameter(
                            $product->mnumi_id,
                            null,
                            'previewE',
                            basename($wizardId, '.pdf'),
                            1,
                            400,
                            400
                        );

                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_parameterPreview',
                            $wizardParameters->signatureManager->encodeParameter($parameterDecoded)
                        );
                        $this->context->cookie->__set(
                            'wizard_' . $id_product . '_' . $wizardId . '_signaturePreview',
                            $wizardParameters->signatureManager->generateSignature($parameterDecoded)
                        );

                        /* END PREVIEW */

                        return $redirectUrl;
                    }
                }
            } else {
                $sessionResultArray = $this->getCalculation($_POST, Tools::getValue('PRODUCT'));
                $product = RestClient::get(
                    $this->new_api_url . 'products/' . Tools::getValue('PRODUCT') . '.json',
                    null,
                    $this->new_api_key,
                    $this->new_api_key
                )
                    ->getResponse();
                $this->context->smarty->assign(
                    array(
                        'resultArray' => $sessionResultArray,
                        'simple' => (isset($product['simple_calculation']) && $product['simple_calculation']) ? true : false
                    )
                );

                return $this->display(__FILE__, 'view/mnumiCalculation.tpl');
            }
        }

        /**
         * MnumiWizard hook
         *
         * @param $params
         * @return mixed
         */
        public function hookMnumiWizard($params)
        {
            if ($id_product = (int)Tools::getValue('id_product')) {
                $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
                if ((!Validate:: isLoadedObject($product)) || (empty($product->mnumi_id))) {
                    return;
                }
                $wizard = RestClient::get(
                    $this->new_api_url . 'products/' . $product->mnumi_id . '/wizard.json',
                    null,
                    $this->new_api_key,
                    $this->new_api_key
                )
                    ->getResponse();
                if (!empty($wizard)) {
                    require_once(dirname(__FILE__) . '/classes/WizardParameter.php');
                    //$this->context->cookie->unsetFamily('wizard_' . $id_product);
                    $wizard = $this->context->cookie->getFamily('wizard_' . $id_product);
                    $wizard = WizardParameter::explodeTree($wizard);
                    $this->context->smarty->assign(
                        array(
                            'id_product' => (int)Tools::getValue('id_product'),
                            'wizard' => ($wizard) ? $wizard['wizard'][$id_product] : null,
                            'wizard_url' => Configuration::get('MNUMI_WIZARD_URL')
                        )
                    );

                    return $this->display(__FILE__, 'view/mnumiWizard.tpl');
                }
            }
        }

        /**
         * Add mnumi to product admin
         *
         * @param $params
         * @return mixed
         */
        public function hookDisplayAdminProductsExtra($params)
        {
            if (!Configuration::get('MNUMI_NEW_API_URL') || !Configuration::get(
                    'MNUMI_NEW_API_KEY'
                ) || !Configuration::get('MNUMI_WIZARD_URL') || !Configuration::get('MNUMI_WIZARD_KEY')
            ) {
                return $this->displayError($this->l('The module is not properly configured!'));
            }
            if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
                $this->context->smarty->assign(
                    array(
                        'mnumi_id' => $this->getMnumiId((int)Tools::getValue('id_product')),
                        'products' => $this->PrepareProductsList(),
                    )
                );

                return $this->display(__FILE__, 'view/mnumi.tpl');
            }
        }

        /**
         * Save product with mnumi_id
         *
         * @param $params
         */
        public function hookActionProductSave($params)
        {
            if (!Db::getInstance()->update(
                'product',
                array('mnumi_id' => pSQL(Tools::getValue('mnumi_id'))),
                'id_product = ' . intval($params['id_product'])
            )
            ) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
            }
        }

        /**
         * Get mnumi_id from product
         *
         * @param $id_product
         * @return array
         */
        public function getMnumiId($id_product)
        {
            $result = Db::getInstance()->ExecuteS(
                'SELECT mnumi_id FROM ' . _DB_PREFIX_ . 'product WHERE id_product = ' . (int)$id_product
            );
            if (!$result)
                return array();

            foreach ($result as $field) {
                $fields = $field['mnumi_id'];
            }

            return $fields;
        }

        /**
         * Get all product list for admin
         *
         * @return bool
         */
        public function PrepareProductsList()
        {
            $categoryTmp = $this->mnumiApi->getCategory();
            if (!$categoryTmp) {
                return false;
            }
            $category = array();
            foreach ($categoryTmp as $cat) {
                $category[$cat['id']] = $cat;
            }
            $products = $this->mnumiApi->getProducts();
            if (!$products) {
                return false;
            }
            foreach ($products as $product) {
                if (isset($category[(int)$product['category']])) {
                    $return[(int)$product['category']]['name'] = $category[(int)$product['category']]['name'];
                    $return[(int)$product['category']]['products'][] = array(
                        'name' => (string)$product['name'],
                        'slug' => (string)$product['slug']
                    );
                }
            }

            return $return;
        }

        /**
         * @return string
         */
        public function getContent()
        {
            $output = '';
            if (Tools::isSubmit('submitMnumiApi')) {
                if (!($MNUMI_WIZARD_URL = Tools::getValue('MNUMI_WIZARD_URL')) || empty($MNUMI_WIZARD_URL)) {
                    $output .= $this->displayError($this->l('You must set Wizard API URL.'));
                } else {
                    if (Configuration::get('MNUMI_WIZARD_URL') != $MNUMI_WIZARD_URL) {
                        Configuration::updateValue('MNUMI_WIZARD_URL', $MNUMI_WIZARD_URL);
                        $output .= $this->displayConfirmation($this->l('Wizard URL saved.'));
                    }
                }
                if (!($MNUMI_WIZARD_KEY = Tools::getValue('MNUMI_WIZARD_KEY')) || empty($MNUMI_WIZARD_KEY)) {
                    $output .= $this->displayError($this->l('You must set Wizard API KEY.'));
                } else {
                    if (Configuration::get('MNUMI_WIZARD_KEY') != $MNUMI_WIZARD_KEY) {
                        Configuration::updateValue('MNUMI_WIZARD_KEY', $MNUMI_WIZARD_KEY);
                        $output .= $this->displayConfirmation($this->l('Wizard KEY saved.'));
                    }
                }
                if (!($MNUMI_NEW_API_URL = Tools::getValue('MNUMI_NEW_API_URL')) || empty($MNUMI_NEW_API_URL)) {
                    $output .= $this->displayError($this->l('You must set Mnumi NEW API URL.'));
                } else {
                    if (Configuration::get('MNUMI_NEW_API_URL') != $MNUMI_NEW_API_URL) {
                        Configuration::updateValue('MNUMI_NEW_API_URL', $MNUMI_NEW_API_URL);
                        $output .= $this->displayConfirmation($this->l('NEW API URL saved.'));
                    }
                }

                if (!($MNUMI_NEW_API_KEY = Tools::getValue('MNUMI_NEW_API_KEY')) || empty($MNUMI_NEW_API_KEY)) {
                    $output .= $this->displayError($this->l('You must set Mnumi NEW API KEY.'));
                } else {
                    if (Configuration::get('MNUMI_NEW_API_KEY') != $MNUMI_NEW_API_KEY) {
                        Configuration::updateValue('MNUMI_NEW_API_KEY', $MNUMI_NEW_API_KEY);
                        $output .= $this->displayConfirmation($this->l('NEW API KEY saved.'));
                    }
                }
            }

            if (empty($output)) {
                $output = $this->displayConfirmation($this->l('Nothing has been changed.'));
            }

            return $output . $this->renderForm();
        }

        /**
         * @return mixed
         */
        public function renderForm()
        {
            $fields_form = array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Settings'),
                        'icon' => 'icon-cogs'
                    ),
                    'input' => array(
                        array(
                            'type' => 'text',
                            'label' => $this->l('Mnumi API2.0 URL'),
                            'name' => 'MNUMI_NEW_API_URL',
                            'class' => 'fixed-width',
                            'desc' => $this->l('Define the URL for API2.0.')
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Mnumi API2.0 KEY'),
                            'name' => 'MNUMI_NEW_API_KEY',
                            'class' => 'fixed-width',
                            'desc' => $this->l('Define the KEY for API2.0.')
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Mnumi Wizard URL'),
                            'name' => 'MNUMI_WIZARD_URL',
                            'class' => 'fixed-width',
                            'desc' => $this->l('Define the URL for Wizard.')
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Mnumi Wizard KEY'),
                            'name' => 'MNUMI_WIZARD_KEY',
                            'class' => 'fixed-width',
                            'desc' => $this->l('Define the KEY for Wizard.')
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                        'class' => 'btn btn-default',
                    ),
                    /*
                    'buttons' => array(
                        array(
                            //'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                            'title' => $this->l('Save & Check Connetcion'),
                            'icon' => 'process-icon-save',
                            'type' => 'submit',
                            'name' => 'submitMnumiApiAndCheck',
                        )
                    )*/
                ),
            );

            $helper = new HelperForm();
            $helper->show_toolbar = false;
            $helper->table = $this->table;
            $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper->default_form_language = $lang->id;
            $helper->allow_employee_form_lang = Configuration::get(
                'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
            ) ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
            $helper->identifier = $this->identifier;
            $helper->submit_action = 'submitMnumiApi';
            $helper->currentIndex = $this->context->link->getAdminLink(
                    'AdminModules',
                    false
                ) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
            $helper->token = Tools::getAdminTokenLite('AdminModules');
            $helper->tpl_vars = array(
                'fields_value' => $this->getConfigFieldsValues(),
                'languages' => $this->context->controller->getLanguages(),
                'id_language' => $this->context->language->id
            );

            return $helper->generateForm(array($fields_form));
        }

        /**
         * @return array
         */
        public function getConfigFieldsValues()
        {
            return array(
                'MNUMI_NEW_API_URL' => Tools::getValue('MNUMI_NEW_API_URL', Configuration::get('MNUMI_NEW_API_URL')),
                'MNUMI_NEW_API_KEY' => Tools::getValue('MNUMI_NEW_API_KEY', Configuration::get('MNUMI_NEW_API_KEY')),
                'MNUMI_WIZARD_URL' => Tools::getValue('MNUMI_WIZARD_URL', Configuration::get('MNUMI_WIZARD_URL')),
                'MNUMI_WIZARD_KEY' => Tools::getValue('MNUMI_WIZARD_KEY', Configuration::get('MNUMI_WIZARD_KEY')),
            );
        }

        /**
         * Calculate product price for Mnumi
         *
         * @param $product
         * @return mixed
         */
        public function getMnumiCalculation($product)
        {

            $postArray = array();
            $product['allow_oosp'] = false;
            $default = null;
            $groups = $this->getGroups($product['mnumi_id']);
            $groups['PRODUCT'] = array(
                'group_type' => 'hidden',
                'id' => 'PRODUCT',
                'default' => $product['mnumi_id'],
                'name' => 'PRODUCT'
            );

            foreach ($groups as $name => &$group) {
                if (isset($group['attributes'])) {
                    foreach ($group['attributes'] as $key => $attr) {
                        if ($attr == $group['default']) {
                            $group['default'] = $key;
                        }
                    }
                }
                if ($group['id'] == "SIZE") {
                    if ($group['attributes'][$group['default']] == "- Customizable -") {
                        $default['size_width'] = 1;
                        $default['size_height'] = 1;
                    } else {
                        $groups[$name] = array('name' => $group['id'], 'value' => @$group['default']);
                    }
                } else {
                    $groups[$name] = array('name' => $group['id'], 'value' => @$group['default']);
                }
            }
            if ($default != null) {
                $groups['size_width'] = $default['size_width'];
                $groups['size_height'] = $default['size_height'];

            }

            $sessionResultArray = $this->getCalculation($groups, $product['mnumi_id']);
            if (isset($sessionResultArray['summaryPriceGross'])) {
                $product['price'] = $sessionResultArray['summaryPriceNet'];
                $product['price_wt'] = $sessionResultArray['summaryPriceGross'];

                return $product;
            }
        }

        /**
         * Install mysql tables for Mnumi
         *
         * @param $method
         * @return bool
         */
        public function alterTable($method)
        {
            $sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product %s mnumi_id %s';

            $sql2 = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product %s raw_attributes %s';
            $sql3 = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product %s attributes %s';
            $sql4 = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product %s price %s';
            $sql5 = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product %s price_wt %s';
            $sql6 = 'ALTER TABLE ' . _DB_PREFIX_ . 'cart_product ADD id INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY ( id ) ;';

            switch ($method) {
                case 'add':
                    $sql = sprintf($sql, 'ADD', 'VARCHAR(255) NOT null');
                    $sql2 = sprintf($sql2, 'ADD', 'mediumtext NOT null');
                    $sql3 = sprintf($sql3, 'ADD', 'mediumtext NOT null');
                    $sql4 = sprintf($sql4, 'ADD', 'decimal(10,5) NOT null');
                    $sql5 = sprintf($sql5, 'ADD', 'decimal(10,5) NOT null');
                    break;
                case 'remove':
                    $sql = sprintf($sql, 'DROP COLUMN', null);
                    $sql2 = sprintf($sql2, 'DROP COLUMN', null);
                    $sql3 = sprintf($sql3, 'DROP COLUMN', null);
                    $sql4 = sprintf($sql4, 'DROP COLUMN', null);
                    $sql5 = sprintf($sql5, 'DROP COLUMN', null);
                    break;
            }
            $insert = array();
            $insert['Wysłanie do API nie powiodło się!'] = array(
                'invoice' => 0,
                'send_email' => 0,
                'module_name' => '',
                'color' => '#ff0000',
                'unremovable' => 0,
                'hidden' => 1,
                'logable' => 1,
                'delivery' => 0,
                'shipped' => 0,
                'paid' => 0,
                'deleted' => 0
            );
            $insert['Wysłano do API'] = array(
                'invoice' => 0,
                'send_email' => 0,
                'module_name' => '',
                'color' => '#00ff00',
                'unremovable' => 0,
                'hidden' => 1,
                'logable' => 1,
                'delivery' => 0,
                'shipped' => 0,
                'paid' => 0,
                'deleted' => 0
            );
            foreach ($insert as $name => $data) {
                $db = Db::getInstance();
                $db->insert('order_state', $data);

                $db->insert(
                    'order_state_lang',
                    array(
                        'id_order_state' => $db->Insert_ID(),
                        'id_lang' => 1,
                        'name' => $name
                    )
                );
            }

            if ((!Db::getInstance()->Execute($sql)) ||
                (!Db::getInstance()->Execute($sql2)) ||
                (!Db::getInstance()->Execute($sql3)) ||
                (!Db::getInstance()->Execute($sql4)) ||
                (!Db::getInstance()->Execute($sql5)) ||
                (!Db::getInstance()->Execute($sql6))
            ) {
                return false;
            }

            return true;
        }

        /**
         * @return bool
         */
        public function install()
        {
            Tools::clearCache();
            Tools::clearCompile();

            return (parent::install() &&
                Configuration::updateValue('MNUMI_WIZARD_URL', '') &&
                Configuration::updateValue('MNUMI_NEW_API_URL', '') &&
                Configuration::updateValue('MNUMI_NEW_API_KEY', '') &&
                $this->alterTable('add') &&
                $this->registerHook('actionValidateOrder') &&
                $this->registerHook('header') &&
                $this->registerHook('displayAdminProductsExtra') &&
                $this->registerHook('mnumiWizard') &&
                $this->registerHook('mnumiParams') &&
                $this->registerHook('orderConfirmation') &&
                $this->registerHook('actionCartSave') &&
                $this->registerHook('displayPaymentTop') &&
                $this->registerHook('displayShoppingCart') &&
                $this->registerHook('actionProductSave'));
        }

        /**
         * @return bool
         */
        public function uninstall()
        {
            Tools::clearCache();
            Tools::clearCompile();

            //Delete configuration
            return (Configuration::deleteByName('MNUMI_NEW_API_URL') &&
                Configuration::deleteByName('MNUMI_NEW_API_KEY') &&
                Configuration::deleteByName('MNUMI_WIZARD_URL') &&
                $this->alterTable('remove') &&
                parent::uninstall());
        }
    }
}
