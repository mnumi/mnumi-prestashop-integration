<div id="product-mnumi" class="panel product-tab">
    <input type="hidden" name="submitted_tabs[]" value="ModuleMnumi" />
    <h3>{l s='Mnumi Integration'}</h3>
    <div class="form-group">
        <label class="control-label col-lg-3 " for="MNUMI_API_KEY">
            <h4>{l s='Product'}</h4>
        </label>
        <div class="col-lg-9 ">
            <select name="mnumi_id">
                <option value="">-- wybierz --</option>
                {foreach from=$products item=category}
                    <optgroup label="{$category.name}">
                        {foreach from=$category.products item=product}
                            <option value="{$product.slug}" {if $mnumi_id == $product.slug} selected{/if}>{$product.name}</option>
                        {/foreach}
                    </optgroup>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="panel-footer">
        <a href="{$link->getAdminLink('AdminProducts')}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
        <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save'}</button>
        <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay'}</button>
    </div>
</div>
