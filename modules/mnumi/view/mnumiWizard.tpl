<div class="mnumiwizard">
    {if $wizard}
    <div class="more-views">
        <ul class="mini-blocks-list" id="attachedFiles">
            {foreach from=$wizard item=item key=i}
            <li class="wizard-li">
                <button id="getWizardPdf" class="small mnumiButton" data-parameter="{$item.parameterPdf}" data-signature="{$item.signaturePdf}" type="button">{l s="Preview"}</button>

                 <!-- if preview hide opts -->
                <button id="editWizard" class="small mnumiButton" data-parameter="{$item.parameterEdit}" data-signature="{$item.signatureEdit}" data-id="{$item.value}" type="button">{l s="Edit"}</button>
                <button id="removeWizard" class="small mnumiButton" data-id="{$id_product}" data-wizard="{$item.value}"><i class="icon-remove"></i></button>
                <input type="hidden" value="{$item.value}" name="order[wizard][{$i}]">
            </li>
            {/foreach}
        </ul>
    </div>
    {literal}
        <script>
            $('button#getWizardPdf').on('click', function (e) {
                e.preventDefault();
                var url = '{/literal}{$wizard_url}{literal}app.php/pdfE/low';
                var url = url + '?parameter=' + $(this).attr('data-parameter')
                              + '&signature=' + $(this).attr('data-signature');                  
                redirect(url);
            })
            $('button#editWizard').on('click', function (e) {
                e.preventDefault();
                $(this).attr('disabled', 'disabled');
                var $form = $('<form id="wizard-edit-form" method="POST" action="{/literal}{$wizard_url}{literal}app.php/editE" target="_self">');
                $form.append('<input name="parameter" value="' + $(this).attr('data-parameter') + '" />');
                $form.append('<input name="signature" value="' + $(this).attr('data-signature') + '" />');
                $form.appendTo($('body')).submit(); 
                setTimeout(function() {
                        $(this).removeAttr('disabled');
                }, 5000);
            });
            $('#removeWizard').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: baseDir + "/modules/mnumi/mnumi-ajax.php",
                    data: { method: "removeWizard", id_product: $('#removeWizard').attr('data-id'), wizard: $('#removeWizard').attr('data-wizard')},
                    complete: function(xmlHttp) {
                        if(xmlHttp.status == 200)
                        {
                            redirect(xmlHttp.responseText);
                        }
                    }
                })
            });

        </script>
    {/literal}
    {else}

        <a href="#" type="submit" id="addNewWizard" data-id="{$id_product}">
            <img alt="Wizard" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAABIAAAASABGyWs+AAAACXZwQWcAAAAYAAAAGAB4TKWmAAAGfElEQVRIx6WVWWic1xXH//d+y3zfaEaj2Uer7Up2YsuSXKtuSfog7JY8OHEpLcWl0Do0dUigC5gQSF9S+tISik36XEJDW2hqcJPUNViJ2ypSFst2iCzLki1ZY2k8mkXfSLN8+3JvH6QY21Ao5MDlPhzO+Z2Ncwi+oGhaDYSQnfVG41Xbtn8NID+4b+99vfhFAaVyBaIowDDMJwWBDtqOk39Q/z8Bv3ntd3A9D5lUSmg2GqJhmjwUCnmWZfHnTj6HXb2924Ay2trCFc55wXO9Ay1dP3/ih8/izT/98WEAf+EFjF+4gL50OixsbAz6eqsnPzr61Uq9vseputFmo+nZtl21LGvx5VMvzT599Jl527aLT33ziHXiR89a+/bvv1opl/eDirQtGmEPZcBOngSOHiUjlcqRJcM4xRT1ia6lxWjMtsTbsTjK1Qoam5twHRee58JxHNe2zKppmPMHDxycnp29/qHvB1UA34gn4nFBEGufA4h/6hTE06dRf/HF739aqZy595VDOadSRvqjj0ANA9ORKK6FFJiEQA7JUEIKZFkGFSgAgBIKAI4gCEYylZLTmcx5AAt38/mJN974wwSZP3wYiiD03jPN8dmRA4/jwJeRv7WAxclJDM/dwHAshra9e7HS2YkZx8U6YwipYaiqAkVRoagqotEoMpk0isU1LN6+hWazidXV1b/Mzd34sehPT6PlOIevZDK7CqOHwBcWUG80oFsmdocUdA8MAPU6Bvv78a2xMZydnMIV3wfnHBwcnDPouo6NjRquz8ygUqm4zUbjnXqj/qtcNuuKA4YBCgR/64ifvZXP/8C1LGpZJjihuBWLIRtPwGq24BcKeGxoCMd29KFWKGKNCmg1WyiXStB1HTVNY81ms5ZIJvOpVOpnQcAqVOAgfKsXbQe7use8ROItVVEilBAIkgRFDiEdiaArmURPVxc6EgnohoH5lbsoVKuwLQu2bUMNh7HnsT13a9rGy5Ik/TwabT8hydLy718/A5FsAQysFS/tazbe9HKdz0c7O8VcrpN09/QgnclCEAWsVav48JNPUCqtwXUcCJRCUVQMj4zg+PHj6B/o1z/++PL038+dqzuO/bjj2Mv3x7R47Bi6BgfZ7D/eDU1Y9sXXVu5+wBj7nmkah5bv3IGut+A4NiRRQiIeh6qG0RaJIJvJYnhkGIlkEkuLS974xYv10dHR29lc9uBnn81cqNVq24BaDasTE7FYJnvwadOs/vTy5dNjO3ZIhZWVQ+FwGAO7d6MzlwMIgWXbaLV0OLaNcrmEIPBxZ2kJruuE3h8fpz95/uQ10zCeOv/uO2IikfBFAIiVy5DicY0B/2zz/cZvh4aDRDyRskwLkiSDEIp6s4XSWhHVSgWWacLzPLTHYmj19qGruxuqosSHhoeSnLFPi8Wi+P6lSyFCCKMAYJomyqWS5G1sdDqt1tIr12dINpv9kiiKkGQZajgMWQ5BEEQABBwA5xxBEGw/hoCxjvb29p1Tk1NSo96YPn3mdQMAE/8MoOH78ILgiBwKfVdh7F+//Otbca+jY0CSZMiyDEVRQSiBIAiglIKSrdHwfR++5yHwfXDG1ZCiDF+9cvU9NayWtoIBaE5RMLa+Dl0QvtPy/bgdCn19v7Y+IstyjyTLCIfDUFUVoiCCbgOwDQiCAJ7nwQ98MMYAjicc1zF7+/r0RDKxtey440AnJBfZuXNXwzCQJuREZmlp741oe4RSikQqhWQ6jfrmJiilINvOQQhYEMD3PPieD8YZtJr2tampyQNvv32uAsAEwOi/OQcFvp3WtCEhHIbnOOHSzZv91fV1RihFKKRAVZT7yw0EICAgABhj8HwPvu+Dc0DTNGlzc3MYQD+ADgCC+AsArwBno7rujqZSr9rZbF/v8nL7SnFt3kqldgmCEAUhCIIAjLEt19tJcM7hex50veVV1ytrc3Nz/+GcXwPgAFuzILwECEkgUxAEl/X0GBpjuY4gkI9wvj5bXZ8oEORlSfLq9XpQ0zRf13XXtizDcV3Ndd28YRhXCvdWL968OTduWeY4gGsANAAGAEY4QBaAlCYImWYiQeqRiChSShuMxc/WavZ7zaajKIoai8UihNCw73vUdVzPdR3T9TyTMeYA8AAwAAUAmw+eXqIBoIBYAmJlSfIzqVRzqFQCgCgAaavq4J9f1gdtH/kBoAnAfQjAtvu2AdBVgFkAf3JLRx8x/n+EPRIE/gtbSz+VbTP6wwAAACV0RVh0Y3JlYXRlLWRhdGUAMjAxMC0wMi0yMFQxMzoyNjo1Mi0wNTowMPKFmmAAAAAldEVYdG1vZGlmeS1kYXRlADIwMTAtMDItMjBUMTM6MjQ6MzAtMDU6MDD4MSTHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAABJRU5ErkJggg==" />
            mWizard
        </a>

    {literal}
        <script>
            $('#addNewWizard').on('click', function() {
                $.ajax({
                    type: "POST",
                    url: baseDir + "/modules/mnumi/mnumi-ajax.php",
                    data: { method: "addNewWizard", id_product: $('#addNewWizard').attr('data-id')},
                    complete: function(xmlHttp) {
                        if(xmlHttp.status == 200)
                        {
                            redirect(xmlHttp.responseText);
                        }
                    }
                })
            });
        </script>
    {/literal}
    {/if}

    {literal}
        <script>
            function redirect(url)
            {
                var ua        = navigator.userAgent.toLowerCase(),
                    isIE      = ua.indexOf('msie') !== -1,
                    version   = parseInt(ua.substr(4, 2), 10);
                if (isIE && version < 9) {
                    var link = document.createElement('a');
                    link.href = url;
                    document.body.appendChild(link);
                    link.click();
                } else { 
                    window.location.replace(url);
                }
            }
        </script>
    {/literal}
</div>