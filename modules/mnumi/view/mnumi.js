$( document ).ready(function() {
    $(document).on('change input', '.attribute_mnumi', function (e) {
        e.preventDefault();
        requestMnumi = '';
        $('.mnumi #attributes select, .mnumi input[type=hidden], .mnumi input[type=text], .mnumi #attributes input[type=radio]:checked').each(function (a, e) {
            requestMnumi += '/' + $(e).attr('name') + '=' + $(e).attr('value');
        });
        requestMnumi = requestMnumi.replace(requestMnumi.substring(0, 1), '?');
        url = window.location + '';

        // redirection
        if (url.indexOf('?') != -1)
            url = url.substring(0, url.indexOf('?'));

        // set ipa to the customization form
        window.location.hash = requestMnumi;
        getProductCalculate();

    });

    $(document).on('click', '.product_quantity_up', function (e) {
        e.preventDefault();
        setTimeout('getProductCalculate()', 500);
    });

    $(document).on('click', '.product_quantity_down', function (e) {
        e.preventDefault();
        setTimeout('getProductCalculate()', 500);
    });

    $('.our_price_display').removeClass('our_price_display');
    function getProductCalculate() {
        alert($('#buy_block').serialize());
        $.ajax({
            url: baseDir + "/modules/mnumi/mnumi-ajax.php",
            data: $('#buy_block').serialize(),
            type: "POST"
        }).done(function (data) {
            $("span#our_price_display").html(data);
        });
    }
});