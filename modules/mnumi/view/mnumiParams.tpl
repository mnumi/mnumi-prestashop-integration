<div class="product_attributes clearfix mnumi">
    <!-- quantity wanted -->
    {foreach from=$groups key=id_attribute_group item=group}
        {if ($group.group_type != 'select')}
            {if $group.id == "QUANTITY"}
                <!-- quantity wanted -->
                {if !$PS_CATALOG_MODE}
                    {if isset($group.static) && $group.static === true}
                        <fieldset class="attribute_fieldset">
                            <label class="attribute_label">{$group.name|escape:'html':'UTF-8'} :</label>
                            {assign var="groupName" value=$group.id}
                            <div class="attribute_list">
                                <select name="QUANTITY" id="group_{$group.id|intval}" class="form-control attribute_mnumi no-print">
                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                        <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div> <!-- end attribute_list -->
                        </fieldset>
                    {else}
                    <p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
                        <label>{l s='Quantity:' mod='mnumi'}</label>
                        <input type="{$group.group_type}" name="QUANTITY" id="quantity_wanted" class="text attribute_mnumi" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
                        <a href="#" data-field-qty="QUANTITY" class="btn btn-default button-minus product_quantity_down">
                            <span><i class="icon-minus"></i></span>
                        </a>
                        <a href="#" data-field-qty="QUANTITY" class="btn btn-default button-plus product_quantity_up ">
                            <span><i class="icon-plus"></i></span>
                        </a>
                        <span class="clearfix"></span>
                    </p>
                    {/if}
                        
                {/if}
                <!-- minimal quantity wanted -->
                <p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
                    {l s='This product is not sold individually. You must select at least' mod='mnumi'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b> {l s='quantity for this product.' mod='mnumi'}
                </p>
            {else}
                <p {if $group.group_type == "hidden"} style="display:none;"{/if}>
                    <label>{$group.name|escape:'html':'UTF-8'} :&nbsp;</label>
                    <input type="{$group.group_type}" name="{$group.id}" id="{$group.id}" class="text attribute_mnumi" value="{$group.default}" style="border: 1px solid rgb(189, 194, 201);"/>
                    <span class="clearfix"></span>
                </p>
            {/if}

        {/if}
    {/foreach}
    {if isset($groups)}
        <!-- attributes -->
        <div id="attributes">
            <div class="clearfix"></div>
            {foreach from=$groups key=id_attribute_group item=group}
                {if ($group.group_type == 'select')}
                    {if isset($group.attributes) and $group.attributes}
                    {if $group.attributes|@count}
                        <fieldset class="attribute_fieldset">
                            <label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$group.id|intval}"{/if}>{$group.name|escape:'html':'UTF-8'} :&nbsp;</label>
                            {assign var="groupName" value=$group.id}
                            <div class="attribute_list">
                                {if ($group.group_type == 'select')}
                                    <select name="{$groupName}" id="group_{$group.id|intval}" class="form-control attribute_mnumi no-print">
                                        {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                            <option data-value="{$group_attribute}" value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{if $group_attribute == "- Customizable -"}Rozmiar niestandardowy{else}{$group_attribute|escape:'html':'UTF-8'}{/if}</option>
                                        {/foreach}
                                    </select>
                                    {if $groupName == "SIZE"}
                                        <div id="SIZE" style="display:none;">
                                            <p class="size_form">
                                                <label for="order_size_width">Szerokość</label>
                                                <input type="text" value="100" id="order_size_width" style="border: 1px solid rgb(189, 194, 201);" name="size_width" class="text attribute_mnumi" disabled> mm
                                            </p>
                                            <p class="size_form">
                                                <label for="order_size_height">Wysokość</label>
                                                <input type="text" value="100" id="order_size_height" style="border: 1px solid rgb(189, 194, 201);" name="size_height" class="text attribute_mnumi" disabled> mm
                                            </p>
                                        </div>
                                    {/if}
                                {/if}
                            </div> <!-- end attribute_list -->
                        </fieldset>
                    {/if}
                    {/if}
                {/if}
            {/foreach}
        </div> <!-- end attributes -->
    {/if}
</div> <!-- end product_attributes -->
{literal}
<script>
function disableAddToCart()
{
    $('#add_to_cart button').attr("disabled", "disabled").hide();
}
$(document).ready(function() {
    $('.product_attributes:not(.mnumi)').remove();
    if($('select[name="SIZE"] option:selected').attr('data-value') == "- Customizable -")
    {
        $('#SIZE').find('input').prop('disabled', false);
        $('#SIZE').show();
    } else {
        $('#SIZE').find('input').prop('disabled', true);
        $('#SIZE').hide();
    }
    disableAddToCart();
    getProductCalculate();

    $('select[name="SIZE"]').change(function () {
        if($('select[name="SIZE"] option:selected').attr('data-value') == "- Customizable -")
        {
            $('#SIZE').find('input').prop('disabled', false);
            $('#SIZE').show();
        } else {
            $('#SIZE').find('input').prop('disabled', true);
            $('#SIZE').hide();
        }
    });
});

$(document).on('change input', '.attribute_mnumi', function(e){
    e.preventDefault();
    disableAddToCart();
});
$(document).on('change input', '.attribute_mnumi', debounce(function(e){
    e.preventDefault();
    requestMnumi = '';
    $('.mnumi #attributes select, .mnumi input[type=hidden], .mnumi input[type=text], .mnumi #attributes input[type=radio]:checked').each(function(a, e) {
        requestMnumi += '/'+$(e).attr('name') + '=' + $(e).attr('value');
    });
    requestMnumi = requestMnumi.replace(requestMnumi.substring(0, 1), '?');
    url = window.location + '';

    // redirection
    if (url.indexOf('?') != -1)
        url = url.substring(0, url.indexOf('?'));

    // set ipa to the customization form
    window.location.hash = requestMnumi;
    getProductCalculate();

},500));

$(document).on('click', '.product_quantity_up', debounce(function(e){
    e.preventDefault();
    getProductCalculate();
}, 500));
$(document).on('click', '.product_quantity_down', debounce(function(e){
    e.preventDefault();
    getProductCalculate();
}, 500));
$(document).on('click', '.product_quantity_down', function(e){
    e.preventDefault();
    disableAddToCart();
});
$(document).on('click', '.product_quantity_down', function(e){
    e.preventDefault();
    disableAddToCart();
});
$('.our_price_display').removeClass('our_price_display');
function getProductCalculate()
{
    var blockBuy = $('#buy_block').serialize();
    if(blockBuy.search("PRODUCT") != -1) {
        disableAddToCart();
        $("span#our_price_display").html('<img alt="" src="data:image/gif;base64,R0lGODlhMgAyANU3APf39+/v7/Pz8+vr6+fn59/f3+Pj49LS0tvb287OztfX18rKyrq6usbGxsLCwqamppKSkr6+vra2tp6enq6urqqqqrKyso6OjqKiopaWlpqammlpaX19fXl5eYqKim1tbXFxcUlJSYaGhmFhYWVlZXV1dVlZWYKCglVVVV1dXU1NTT09PVFRUUVFRUFBQTk5OTExMSgoKDU1NS0tLRwcHPv7+////////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUI4NjExM0VDQTc1MTFFMUI5Q0Q4ODFDMzUxMUU3NDIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUI4NjExM0RDQTc1MTFFMUI5Q0Q4ODFDMzUxMUU3NDIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIFdpbmRvd3MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmRpZDpDMzQ2MzYyNTYxQ0FFMTExOUUwQ0M3NDhBRTQ0OUIyQSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDMzQ2MzYyNTYxQ0FFMTExOUUwQ0M3NDhBRTQ0OUIyQSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUKADcALAAAAAAyADIAAAb/QJtwSCwaj8ikcslsOp/QqHRKrVqhBATAVoOkDEKDonZt1h6gAtfztQEgHUEZOTgEhBiWY20CEzYfcjUAZHNCCxsWQgkkGTU1IihgCyYTZDUCcoY2AxwjCjYBICABkCwENhMhe5gCW1cBWosoIoQTHgM1DBgCNRMcYAABr27EUQcgE1tdLAucBcY1BgaXhELCmlIBGijKNgUPB2avwsNVAKoZmoVOAgQDxk6xmugdoFGY8EMA8UkHHxwSCJF2Rwq/gQEE5GoSwMKHERnUXBkUgIC1JwMebHjQD5/Cjk0KIGBHBYCrJwYoSIC2iUi5bEcOcADYgcKATTUIEChQoCCS/xoDFFDgoAHBJgDTXJFUggnklEFLW0q18WCChgkTFkSt4o4nTyUzSZDYQGErlVgHEqhVEmCAW30tH0GdSteGAAURIoCZA9St0yEEKlzgkGxvGaAKEiDoqaQABAgRFPg01FUBqiRNh2Ay+2QQkUdPDmBw8Jchz9JFpFnoIELgFQEFEixYGDLDBQk3uXA2U2cBtSYGGki0ocCC4c4Ixlx7txuJAhEZckOpgYCB8ioHPGQIZmDyz4RcEDg4gBoJgQsX9iaYAGruZzIADhzQVH14lAENLg+YgOGOAvKhoEIdeTUUwIBESElXRQ0RXGBUDRRQ0IsDD8iRgE12RdAATGUYgEwBA1sEUIEEj0iQwR0GWGCUDQY4cN0cBCxQkGji6DLBHQIw4MAlPDWHz0AJYACGjXcAEAGIdX02gCw1UFiQAQX4mOSUVFZp5ZVYEhEEACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSbMLJLSAMFIrQqHRoYt4gIeuNNO0SBTeAkNE6CLFMQYmIeHqhhdSQAFLeLtmbgkggQd5QYDcGYTcdYhcuTBYvZjccKwmAYQZINwhXQhUcQg5DEBtMCit/QgOEU4QVRCF7YlMCICYDQgksGl0AD0JITJheARoRQgcuKm5TNrt2k0QOISYEUgABlpaozUInI7RTJRfIAa/ZNwRaNVAADlwY0uSwWkUW78lvA+70ROhQDAvV+W9ePSkh4oYEQQCjnEOGL6EQAgO6QbHksKJFBhUyUnBk8ca+h0RAdBwiZsEQGyhTjixCceWbGgQkSaxICGEACUI23JjpENW9iHI3JjQg8HFkgAFuUOpzKYVABQYdGxIJwOBEKYsFFCAsEmGrRWxNFEg00CBeNrBeDFxgRs6AJ0BPMDBpSa4AXSgTNHRTYCFpF5P7CEktIuCAFV0UxCADdNdLgweoJEBlupMIAAvDbjTAMM5dAI7Z4hGggOrxqwUmTQ1+p8HK5lcNKA8x+YpnlyAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBqJhKNyWdwsP8zoseGUWpUlRrQwqlyZl9Ig2hp6VIjveLjBCC0iYeIhnJSImu8tPnyMrh16QyRegoU3CzcpVgx/gkMLK3oMfI83IEQDSUQneZZLAA8hlXIcHRQBn0QAESkvJwZYh6oNJhuJqlcDB0cOuVICDQ8OAxcfFxe+v0QWGyQsLRRDDhl0y0MZJRgGAtfe38wS4uBCAgHnAR6mHaTeBgjwaeTzvwEKCvREDhgXJ7PX78wJefAPHAABBNbkkyKhwTIAURpAeCAvVw0DBAocqYABlzcABLpFSfBJoZBUSwY8kPaJlxUCDywMQamHgMsoFlgKUSYFHpFMm0sQQHwjQcgAjTeGCkE6QIFCmlYOMFjjwOWBojcQaBFyAKgeCx4dkLyRQOaNADxvFIj1CN8Qh3LM3lDg9ReCQ2WHdCWHdG6ETFeCAAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsDoaWT3HJbDY7wwdpOIE6r8RCUSGUDj2noQHr5DSEA0+lOxWWLMMOhszEeIYniBCTGpqGECh0TAQcazcRGUhdRIJER1cQDEMUSnQiG0QkD1gSHxFDDGN0C0MolmQgcINFByqZTgJFEJCsQndXBROHtmQUN3pFIhmgvVchF00OF8XGxgHOrAkI0balExcZGKXVSw4lfytFDxHQ3UQiJma157Ht1QkOERENWu9CBRoXEPwPF/8azty7kYDFixUrWgxcyLAhhQkJZAlZQAFDBgcS33lAyKJDOQYMDtywwZCAAw4pFDUcFCCByHYAMi5JQEHUuwEAzD1yIIHLQm8BNmTeGDXEpzEBOm8AwGLBQTQBBNhdkcAtWgACSZU5FULgQNYmBggMEVBAaBEARG80qCr1BgGxQgyYHVTg5Y2qBRLE1YvPXi8BDqgJOeATQVW7OdvS8RsAASQEfIf6jdluEj6BNxD4bZiXyFwmQQAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BIJAqKoqJyyVw6kkPQ8HFpWpuHUyE6zHiuYIKGmGFwhSIzugJeUjLHG2Qy/AgdpOEk1WZ6JFMARRZVQhtsfTcPDkQnVgFDFxyJQg1Qd32AQgojfQseapRDfIkNiKJyk00EFBaoS5BFY0QIGhivTR8sTQu3uEQJGx2/xEMKxZQUYxQHyEUnIyYqtJrOQhcjl9bb3AsLDQncNwS+NxoUGBgTD+I3Ju3w2wQG3NVDBw4MFQvcGUQNNxwAjDekHMGDlT6cUoKAwcBiBkqsGMHPSIKH1hywCEGnSYAtrx4YpGCviABwuDLE8AcmQINmvybIgBAHS0Uh9NoEiHUjwww1g7BoHbshoCYToxJuhkFgjMASpkOcvhoqpADIIgVqAjCaSCpOJgPEEVAaTgiBsPEIUK2RKAgAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBIjF6NyyRwuMkNJUiiBNq/CAdF6s0xvmAkW+zgMKQ6hNztZDDOSsTFCGWLqXZFwwTEIKx1ySg9pNwkRc2JCJYI3AwxmQg1cAU0THkQdDFcJGJEAkVgRCkMcHlpXBw9ujZlcY5utQg8QlUwNDbJYEh8PRhKjukoLIhsaSwoRCcJFHF/M0DcEANFNCrEOEgwRBdVECx4lIylFDaTeQholr+jt6AUH8Qqo7RU3GscNEvsO5+4tIVqgcEfQna12vogYWLDAAQJ3E0LcSHEhgQAECroVFJIBxEZBAAwQqLYszhICBxAcjGbiRqghBhCMbHdgw0AiAKgRESAMzxFqaxplYbihCIsCP40AJJh5Q8WYAkFv8MQygEQgIRNY2BNkAOmNGkYqZOD54EUsWQEE6CQwdQBSDS8e3iDBgl6jAkwHzHS7J0SsBCqOyZoqJGcWpAM2XL2hLVoAf3yFdBixMhoAwje8NpQTBAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsEhcPo3LJHB6SwggxgmlalQjoTTqUaK9MBlEcJX4fXDCx4RhSuBGI8CAXSiAEtTFyMBYWQg1aF2RqgE6FVhUaRCJXBRZEBWoJekUWfZZDBSIXegqHmjcWE00CB5lCNqJKDhxGBA0NrEt9HRRLk7RGHhm7v0MGwGAOCjcLDg2pw0QnIB8SQwQHusw3DycTy9bc1gYFBnndQhgV5gkOEQ4J1cwFHywmJiPj9fbMEghFCggLwt0oUHzwJaSAuHu37ilc8qpJO2AnWCwZYCBAvRS4mAD4ldHOLCYHWa1QA6DiryRVrAQIaaPGFQEfzLTQNGBAFAgWCxgbVeoGhTWZwwCU2GDxgoohLojQYxXA4g0CJE4IuRDCab0GKMhAqLqkzS8NK3RRtTrOQYUhGVQ43dYkCAAh+QQFCgA3ACwFAAUAKAAoAAAG/8CbcEgsFi3GpHJJlAwTSOEiwqwqGQFhojJ0UK1MwcIwPBy0UQFDMWQ0wEbFWchYCBuPIaa7hxcBCUQISQxCAxMOfjcIBUMIdkIATBIURBOQTA1khn6NNwQTeVYGDZ6KQgQaomAGCpKnNw6VRAJFBAavsEsJGoVFCgUDukoVvUoECJvDQw+ry8/QYI9CCHI3wtFDCg8aHh5fNwHB2UMUEBaD5OrrWUO1605CFgVmB+PrHB8fJCDr/v/q3hQZYGAAgnbkKIAowWHIgAEI/Z35BtAPgHfQIIAzAiBAxGceQJxIIgCjOg4l4ik54EGZIgYqrSxwgYLAsBBwHKwwsemAzVJ/BVSw2MQAxcYlHYqg8KWkQAVsAVSQqNWgz08jJog4mANmQghMQzKgEIJBxRAQG3QFbRjug4ckY4dsmKWowARsN1IoASFiXZ8kJz6sSxAzK6wgACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSEQ6jcskcGhZDBWOISDSvyoFVqEgKDw0sFjEYEgpcInSYOIiNBfQXwY0MGQJhQhJ4GwkGQgZlNwNyCGs3DG5+A4Q3gE01Qw1hQgMSjEwDCo95YgpEFFNYAXR+RhYSjY+oNwleTAJ9rmIPlkMCg5+1cBa3SjYBrb1CBBISBFe8xUK0RQEAzag1AwYGBMzTQxIUExOJAATS20QRD6Tl6utKAe7a6g0R83Zxg+xDGhwnHiL4/+sQWMAQaF2CBsqMXUgRAsYHOeoYnOCQwQKCAyNIWAQohIEGjn4QdLigDtwSAh5eoEg3bQIHkkUwutDwrFwBCP6OVIB4A0IxZwexbizYssTEhmIRNlB4EwIEMSwFaDEosRQLiUcjiDYRMWFIBBBB/xQx8eFKhY4fErmqgGLIiRJCug5JMYRDh2Ywb5TIIEQDiyEjLCng0IvAgyEHjg4x4dYnO7ZE/grxcAKkEBK9ggAAIfkEBQoANwAsBQAFACgAKAAABv/Am3BILBYTxqRyqSwghwimVKkYFoiIw3RqWD5v0a10MCB2hY2hg5gWu4naN7Ms/t4i2yuxlicy1lsEdHJCf4SHCnaHiwgSSQEAi0oODIpEApJFDW2ZmRgSg51UYCguKSYaAaJEDhIVD1oABxAmIGGrNxINZ0WRuL+LEMLCDMCYQw0qLisrLRDANxgeFzca0Ne4FdhDqts3Fx4Wxd5WNw/kYhmiBedL6r8PGRh6RSrXExqA6FskmSDjUhKM2CAKj5QSRQhu03djRAchCSgYiWBByJo4NyxJSUGEg5AHCiUO8eBhEcchJ4iMGPKh3CKRaoRMaHlDwLMhGgmFGDLh5I0Jd7+qDKmAkFAQACH5BAUKADcALAUABQAoACgAAAb/wJtwSCwSBwijcskcBpJCApFgaFqX1ehhOChcrwIiIKAdGqTCQfZLFISFhkE0URYWFOxbAC0EyPU1QgJ/Bl5CB2tXChsQbWwFhnoLkUsXNBlDNm9WA2SHdFcQMR55RQEJoEoCiRkplKV4TAsoG1tRpUwGDrGCFS0uEIm4RwmTSgEQKiLDRgMHB55LCrbM1dZKfzcjIUIP10YECwsODEUj30YHC9ToTTbt1RhEDfBDCgnPByZEGvVCEhgCVvBHEF4VCwQPZLuhYYOQDv4aVJgQoYEBBCBA3BBWL4EEdgW/TGgngZcSFiPofRPAAEMFPnYcFqRAIdVGCUQQeGO2wIhJYiMorDW4oNIKHo1CoJQaygbpDQcc2Lgc0sBDzyYMopVYdkNBOT1rvi5oZM0phAtCKEC8IaHEEA2YmIEYKYTsjQcf4JyglqGoNRFXdw4ZaO+bhbzdEN/AELfe1RsWotoBySQIACH5BAUKADcALAUABQAoACgAAAb/wJtwSCQSCkQDsVZsOp/CDGowVA4F0GzxMLSEEtUhgKrVKlQYoWEDCQsFATG5XASxCMLNB3tDCANWQnh0TQohHkIaJINDA4x+hEMaQyIrXH1ajH2aThoyQwQaYHQAQwVIZQ8wbZFFCJCEJK1GgU4HIx2ws1ABCqhDEiy7WqeckjcXw00Dv8rOQhXPhANUGy8kG9JOrwcLgRgbHNpECL7j5+jpWqVEIyoqISoP6QENEvcM6vrjEfnoCrpuPAAxIsSJdAokUDhw6YSIfYIUNIAYCUI0bQuaFcFAosMoaQ0YRHByggSFdAMYLGjiAN0lIaeyfEg27MA8QhsyxGk1J8GEWY9P8IjYuQARHQr+biTQoEDLREoZhjRgQuTA0wOThiCopYWBBysTsjKg6YDmjQoXd3VIe1YIBaM3IEAiwLZV0gIXLkkwS8FCuggluogTYuHmuWYMWAkJCCUIADs=" />');
        $.ajax({
            url: baseDir + "/modules/mnumi/mnumi-ajax.php",
            data: $('#buy_block').serialize(),
            type: "POST",
            beforeSend: function () {
                $('a.product_quantity_down, a.product_quantity_up, .mnumi select, .mnumi input[type=hidden], .mnumi input[type=text], .mnumi #attributes input[type=radio]:checked').attr("disabled", "disabled");
            }
        }).done(function (data) {
            $("span#our_price_display").html(data);
            $('#add_to_cart button').removeAttr("disabled").show();

        }).always(function () {
            $('a.product_quantity_down, a.product_quantity_up, .mnumi select, .mnumi input[type=hidden], .mnumi input[type=text], .mnumi #attributes input[type=radio]:checked').removeAttr("disabled");
        });
    }
}
function debounce(fn, delay) {
    var timer = null;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            fn.apply(context, args);
        }, delay);
    };
}
</script>
{/literal}
