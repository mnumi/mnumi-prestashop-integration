<div class="content_prices clearfix">
    {if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
        <!-- prices -->
        <div class="price">
            <p class="our_price_display" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                {if $product->quantity > 0}<link itemprop="availability" href="http://schema.org/InStock"/>{/if}
                {if $priceDisplay >= 0 && $priceDisplay <= 2}
                    <span id="our_price_display" itemprop="price">{convertPrice price=$productPrice}</span>
                    <!--{if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
											{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}
										{/if}-->
                    <meta itemprop="priceCurrency" content="{$currency->iso_code}" />
                {/if}
            </p>
            <p id="reduction_percent" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>
									<span id="reduction_percent_display">
										{if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
									</span>
            </p>
            <p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction) && $group_reduction == 0} class="hidden"{/if}>
                {if $priceDisplay >= 0 && $priceDisplay <= 2}
                    <span id="old_price_display">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction}{/if}</span>
                    <!-- {if $tax_enabled && $display_tax_label == 1}{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if} -->
                {/if}
            </p>
            {if $priceDisplay == 2}
                <br />
                <span id="pretaxe_price">
										<span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span>
                    {l s='tax excl.'}
									</span>
            {/if}
        </div> <!-- end prices -->
        <p id="reduction_amount" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>
								<span id="reduction_amount_display">
								{if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|intval !=0}
                                    -{convertPrice price=$productPriceWithoutReduction-$productPrice|floatval}
                                {/if}
								</span>
        </p>
        {if $packItems|@count && $productPrice < $product->getNoPackPrice()}
            <p class="pack_price">{l s='Instead of'} <span style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span></p>
        {/if}
        {if $product->ecotax != 0}
            <p class="price-ecotax">{l s='Include'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='For green tax'}
                {if $product->specificPrice && $product->specificPrice.reduction}
                    <br />{l s='(not impacted by the discount)'}
                {/if}
            </p>
        {/if}
        {if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
            {math equation="pprice / punit_price"  pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
            <p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
        {/if}
    {/if} {*close if for show price*}
    <div class="clear"></div>
</div> <!-- end content_prices -->
<div class="product_attributes clearfix">
    <!-- quantity wanted -->
    {if !$PS_CATALOG_MODE}
        <p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
            <label>{l s='Quantity:'}</label>
            <input type="text" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
            <a href="#" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down">
                <span><i class="icon-minus"></i></span>
            </a>
            <a href="#" data-field-qty="qty" class="btn btn-default button-plus product_quantity_up ">
                <span><i class="icon-plus"></i></span>
            </a>
            <span class="clearfix"></span>
        </p>
    {/if}
    <!-- minimal quantity wanted -->
    <p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
        {l s='This product is not sold individually. You must select at least'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b> {l s='quantity for this product.'}
    </p>
    {if isset($groups)}
        <!-- attributes -->
        <div id="attributes">
            <div class="clearfix"></div>
            {foreach from=$groups key=id_attribute_group item=group}
                {if $group.attributes|@count}
                    <fieldset class="attribute_fieldset">
                        <label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'} :&nbsp;</label>
                        {assign var="groupName" value="group_$id_attribute_group"}
                        <div class="attribute_list">
                            {if ($group.group_type == 'select')}
                                <select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="form-control attribute_select no-print">
                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                        <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            {elseif ($group.group_type == 'color')}
                                <ul id="color_to_pick_list" class="clearfix">
                                    {assign var="default_colorpicker" value=""}
                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                        <li{if $group.default == $id_attribute} class="selected"{/if}>
                                            <a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}" style="background: {$colors.$id_attribute.value|escape:'html':'UTF-8'};" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
                                                {if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                    <img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
                                                {/if}
                                            </a>
                                        </li>
                                        {if ($group.default == $id_attribute)}
                                            {$default_colorpicker = $id_attribute}
                                        {/if}
                                    {/foreach}
                                </ul>
                                <input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
                            {elseif ($group.group_type == 'radio')}
                                <ul>
                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                        <li>
                                            <input type="radio" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
                                            <span>{$group_attribute|escape:'html':'UTF-8'}</span>
                                        </li>
                                    {/foreach}
                                </ul>
                            {/if}
                        </div> <!-- end attribute_list -->
                    </fieldset>
                {/if}
            {/foreach}
        </div> <!-- end attributes -->
    {/if}
</div> <!-- end product_attributes -->