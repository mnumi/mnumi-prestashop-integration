{if $simple === false}
    <fieldset>
        <label>Format: </label>
        {l s=$resultArray.size.fieldLabel} {l s=$resultArray.sides.fieldLabel}
    </fieldset>
    <fieldset>
        <label>{l s='Issue'}: </label>
        {$resultArray.count.value} x {$resultArray.quantity.value} {l s='pc.'}
    </fieldset>

    {assign var=priceItems value=$resultArray.priceItems}
    {foreach key=key item=rec from=$priceItems}
        {if isset($rec.label) && $rec.label != ""}
            {if 'MATERIAL' == $key}
                {if !$resultArray.fixedPrice}
                <fieldset>
                    <label>{l s=$rec.label}:</label><br />
                    {toolsConvertPrice price=$rec.price assign=price}
                    {$rec.fieldLabel}: {convertPrice price=$price}
                    {/if}
                </fieldset>
            {elseif 'PRINT' == $key}
                <fieldset>
                    <label>{l s=$rec.label}: </label><br />
                    {$rec.fieldLabel} - {$resultArray.squareMetre|string_format:"%.2f"}m<sup>2</sup>: {convertPrice price=$price}
                </fieldset>
            {/if}
        {/if}
    {/foreach}
    {if isset($resultArray.tradersPrice) && $resultArray.tradersPrice > 0}
        <fieldset>
            <label>{l s='Additional work'}:</label>
            {toolsConvertPrice price=$resultArray.tradersPrice assign=tradersPrice}
            {convertPrice price=$tradersPrice}
        </fieldset>
    {/if}
{/if}
<div class="our_price_display">
<label>{l s='Price:'}</label>
{if 0 != $resultArray.summaryPriceNet}
    {convertPrice price={toolsConvertPrice price=$resultArray.summaryPriceGross}}
    <input type="hidden" name="price_wt" value="{$resultArray.summaryPriceGross}" />
    <input type="hidden" name="price" value="{$resultArray.summaryPriceNet}" />
{else}
    {l s='ERROR'}
{/if}
</div>



