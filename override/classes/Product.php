<?php
/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(__DIR__ . '/../../modules/mnumi/mnumi.php');

class Product extends ProductCore
{
    /** @var string Mnumi ID */
    public $mnumi_id;

    /**
     * Get all available products
     *
     * @param integer $id_lang Language id
     * @param integer $start Start number
     * @param integer $limit Number of products to return
     * @param string $order_by Field for ordering
     * @param string $order_way Way for ordering (ASC or DESC)
     * @return array Products details
     */
    public static function getProducts($id_lang, $start, $limit, $order_by, $order_way, $id_category = false,
                                       $only_active = false, Context $context = null)
    {
        $products = parent::getProducts($id_lang, $start, $limit, $order_by, $order_way, $id_category, $only_active, $context);
//        $mnumi = new Mnumi();
//        foreach ($products as $key => &$product)
//        {
//            if(!empty($product['mnumi_id']))
//            {
//                $product = $mnumi->getMnumiCalculation($product);
//                if(empty($product))
//                {
//                    //unset($products[$key]);
//                }
//            }
//        }
        return $products;
    }
    
    /**
	* Get product price
	*
	* @param integer $id_product Product id
	* @param boolean $usetax With taxes or not (optional)
	* @param integer $id_product_attribute Product attribute id (optional).
	* 	If set to false, do not apply the combination price impact. NULL does apply the default combination price impact.
	* @param integer $decimals Number of decimals (optional)
	* @param integer $divisor Useful when paying many time without fees (optional)
	* @param boolean $only_reduc Returns only the reduction amount
	* @param boolean $usereduc Set if the returned amount will include reduction
	* @param integer $quantity Required for quantity discount application (default value: 1)
	* @param boolean $forceAssociatedTax DEPRECATED - NOT USED Force to apply the associated tax. Only works when the parameter $usetax is true
	* @param integer $id_customer Customer ID (for customer group reduction)
	* @param integer $id_cart Cart ID. Required when the cookie is not accessible (e.g., inside a payment module, a cron task...)
	* @param integer $id_address Customer address ID. Required for price (tax included) calculation regarding the guest localization
	* @param variable_reference $specificPriceOutput.
	* 	If a specific price applies regarding the previous parameters, this variable is filled with the corresponding SpecificPrice object
	* @param boolean $with_ecotax insert ecotax in price output.
	* @return float Product price
	*/
	public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 6, $divisor = null,
		$only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = null, $id_cart = null,
		$id_address = null, &$specific_price_output = null, $with_ecotax = true, $use_group_reduction = true, Context $context = null,
		$use_customer_price = true)
	{
            if(Tools::getValue('ipa') || $id_product_attribute != null)
            {
                $ipa = Tools::getValue('ipa') ? Tools::getValue('ipa') : $id_product_attribute;
                $mnumi = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'cart_product WHERE id = ' . $ipa . ' AND id_product = ' . $id_product);
                if((bool)$mnumi !== false)
                {
                    return $mnumi['price'];
                }
            }
            if (!$context)
                    $context = Context::getContext();

            $cur_cart = $context->cart;

            if ($divisor !== null)
                    Tools::displayParameterAsDeprecated('divisor');

            if (!Validate::isBool($usetax) || !Validate::isUnsignedId($id_product))
                    die(Tools::displayError());

            // Initializations
            $id_group = (int)Group::getCurrent()->id;

            // If there is cart in context or if the specified id_cart is different from the context cart id
            if (!is_object($cur_cart) || (Validate::isUnsignedInt($id_cart) && $id_cart && $cur_cart->id != $id_cart))
            {
                    /*
                    * When a user (e.g., guest, customer, Google...) is on PrestaShop, he has already its cart as the global (see /init.php)
                    * When a non-user calls directly this method (e.g., payment module...) is on PrestaShop, he does not have already it BUT knows the cart ID
                    * When called from the back office, cart ID can be inexistant
                    */
                    if (!$id_cart && !isset($context->employee))
                            die(Tools::displayError());
                    $cur_cart = new Cart($id_cart);
                    // Store cart in context to avoid multiple instantiations in BO
                    if (!Validate::isLoadedObject($context->cart))
                            $context->cart = $cur_cart;
            }

            $cart_quantity = 0;
            if ((int)$id_cart)
            {
                    $cache_id = 'Product::getPriceStatic_'.(int)$id_product.'-'.(int)$id_cart;
                    if (!Cache::isStored($cache_id) || ($cart_quantity = Cache::retrieve($cache_id) != (int)$quantity))
                    {
                            $sql = 'SELECT SUM(`quantity`)
                            FROM `'._DB_PREFIX_.'cart_product`
                            WHERE `id_product` = '.(int)$id_product.'
                            AND `id_cart` = '.(int)$id_cart;
                            $cart_quantity = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
                            Cache::store($cache_id, $cart_quantity);
                    }
                    $cart_quantity = Cache::retrieve($cache_id);
            }

            $id_currency = (int)Validate::isLoadedObject($context->currency) ? $context->currency->id : Configuration::get('PS_CURRENCY_DEFAULT');

            // retrieve address informations
            $id_country = (int)$context->country->id;
            $id_state = 0;
            $zipcode = 0;

            if (!$id_address && Validate::isLoadedObject($cur_cart))
                    $id_address = $cur_cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};

            if ($id_address)
            {
                    $address_infos = Address::getCountryAndState($id_address);
                    if ($address_infos['id_country'])
                    {
                            $id_country = (int)$address_infos['id_country'];
                            $id_state = (int)$address_infos['id_state'];
                            $zipcode = $address_infos['postcode'];
                    }
            }
            else if (isset($context->customer->geoloc_id_country))
            {
                    $id_country = (int)$context->customer->geoloc_id_country;
                    $id_state = (int)$context->customer->id_state;
                    $zipcode = (int)$context->customer->postcode;
            }

            if (Tax::excludeTaxeOption())
                    $usetax = false;

            if ($usetax != false
                    && !empty($address_infos['vat_number'])
                    && $address_infos['id_country'] != Configuration::get('VATNUMBER_COUNTRY')
                    && Configuration::get('VATNUMBER_MANAGEMENT'))
                    $usetax = false;

            if (is_null($id_customer) && Validate::isLoadedObject($context->customer))
                    $id_customer = $context->customer->id;

            return Product::priceCalculation(
                    $context->shop->id,
                    $id_product,
                    $id_product_attribute,
                    $id_country,
                    $id_state,
                    $zipcode,
                    $id_currency,
                    $id_group,
                    $quantity,
                    $usetax,
                    $decimals,
                    $only_reduc,
                    $usereduc,
                    $with_ecotax,
                    $specific_price_output,
                    $use_group_reduction,
                    $id_customer,
                    $use_customer_price,
                    $id_cart, 
                    $cart_quantity
            );
	}

    /**
     * Get new products
     *
     * @param integer $id_lang Language id
     * @param integer $pageNumber Start from (optional)
     * @param integer $nbProducts Number of products to return (optional)
     * @return array New products
     */
    public static function getNewProducts($id_lang, $page_number = 0, $nb_products = 10, $count = false, $order_by = null, $order_way = null, Context $context = null)
    {
        $mnumi = new Mnumi();
        $products = parent::getNewProducts($id_lang, $page_number, $nb_products, $count, $order_by, $order_way, $context);
        if ($products) {
            foreach ($products as $key => &$product) {
                if (!empty($product['mnumi_id'])) {
                    $product = $mnumi->getMnumiCalculation($product, $mnumi);
                    if (empty($product)) {
                        unset($products[$key]);
                    }
                }
            }
        }
        return $products;

    }




}

