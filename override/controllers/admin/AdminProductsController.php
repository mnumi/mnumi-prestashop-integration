<?php
/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
class AdminProductsController extends AdminProductsControllerCore
{
    public function __construct()
    {
        parent::__construct();
        if ($this->context->shop->getContext() != Shop::CONTEXT_GROUP)
            $this->available_tabs = array_merge($this->available_tabs, array(
                'Informations' => 0,
                'Pack' => 7,
                'VirtualProduct' => 8,
                'Prices' => 1,
                'Seo' => 3,
                'Associations' => 4,
                'Images' => 9,
                'Shipping' => 5,
                'Combinations' => 6,
                'Features' => 10,
                'Customization' => 11,
                'Attachments' => 12,
                'Suppliers' => 13,
                'ModuleMnumi' => 1,
            ));


        if ($id_product = (int)Tools::getValue('id_product'))
        {
            $product = new Product($id_product,
                true,
                $this->context->language->id,
                $this->context->shop->id);
            if ( (Validate:: isLoadedObject($product)) && (!empty($product->mnumi_id)) )
            {
                unset($this->available_tabs['Combinations']);
                /**
                 * Temporary
                 */
                //unset($this->available_tabs['Prices']);
                unset($this->available_tabs['Suppliers']);
                //unset($this->available_tabs['Shipping']);
            }
        }

        asort($this->available_tabs, SORT_NUMERIC);
    }
}
