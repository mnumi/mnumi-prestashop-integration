<?php
/**
 * Created by IntelliJ IDEA.
 * User: dominik
 * Date: 26.01.15
 * Time: 10:30
 */

class CartController extends CartControllerCore
{
    public $php_self = 'cart';

    protected $id_product;
    protected $id_product_attribute;
    protected $id_address_delivery;
    protected $customization_id;
    protected $qty;
    public    $ssl = true;

    protected $ajax_refresh = false;

    /**
     * Display ajax content (this function is called instead of classic display, in ajax mode)
     */
    public function displayAjax()
    {
        if ($this->errors)
            die(Tools::jsonEncode(array('hasError' => true, 'errors' => $this->errors)));
        if ($this->ajax_refresh)
            die(Tools::jsonEncode(array('refresh' => true)));

        // write cookie if can't on destruct
        $this->context->cookie->write();

        if (Tools::getIsset('summary'))
        {
            $result = array();
            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1)
            {
                $groups = (Validate::isLoadedObject($this->context->customer)) ? $this->context->customer->getGroups() : array(1);
                if ($this->context->cart->id_address_delivery)
                    $deliveryAddress = new Address($this->context->cart->id_address_delivery);
                $id_country = (isset($deliveryAddress) && $deliveryAddress->id) ? $deliveryAddress->id_country : Configuration::get('PS_COUNTRY_DEFAULT');

                Cart::addExtraCarriers($result);
            }
            $result['summary'] = $this->context->cart->getSummaryDetails(null, true);
            $result['customizedDatas'] = Product::getAllCustomizedDatas($this->context->cart->id, null, true);
            $result['HOOK_SHOPPING_CART'] = Hook::exec('displayShoppingCartFooter', $result['summary']);
            $result['HOOK_SHOPPING_CART_EXTRA'] = Hook::exec('displayShoppingCart', $result['summary']);

            foreach ($result['summary']['products'] as $key => &$product)
            {
                $product['quantity_without_customization'] = $product['quantity'];
                if ($result['customizedDatas'] && isset($result['customizedDatas'][(int)$product['id_product']][(int)$product['id_product_attribute']]))
                {
                    foreach ($result['customizedDatas'][(int)$product['id_product']][(int)$product['id_product_attribute']] as $addresses)
                        foreach ($addresses as $customization)
                            $product['quantity_without_customization'] -= (int)$customization['quantity'];
                }
                $product['price_without_quantity_discount'] = ProductCore::getPriceStatic(
                    $product['id_product'],
                    !Product::getTaxCalculationMethod(),
                    $product['id_product_attribute'],
                    6,
                    null,
                    false,
                    false
                );
                if($product['mnumi_attributes'])
                {
                    $product['price_without_quantity_discount'] = Tools::ps_round((int)$product['price'], 6);
                    $product['price'] = Tools::ps_round((int)$product['price'], 6);
                    $product['price_wt'] = Tools::ps_round((int)$product['price_wt'], 6);
                }

            }
            if ($result['customizedDatas'])
                Product::addCustomizationPrice($result['summary']['products'], $result['customizedDatas']);

            Hook::exec('actionCartListOverride', array('summary' => $result, 'json' => &$json));
            die(Tools::jsonEncode(array_merge($result, (array)Tools::jsonDecode($json, true))));
        }
        // @todo create a hook
        elseif (file_exists(_PS_MODULE_DIR_.'/blockcart/blockcart-ajax.php'))
            require_once(_PS_MODULE_DIR_.'/blockcart/blockcart-ajax.php');
    }
}