<?php
/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
require_once(__DIR__ . '/../../../modules/mnumi/mnumi.php');

class CategoryController extends CategoryControllerCore
{
    /**
     * Assign list of products template vars
     */
    public function assignProductList()
    {
        $hookExecuted = false;
        $this->context->smarty->assign('categoryNameComplement', '');
        $this->nbProducts = $this->category->getProducts(null, null, null, $this->orderBy, $this->orderWay, true);
        $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
        $products = $this->category->getProducts($this->context->language->id, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay);
//        $mnumi = new Mnumi();
//        
//        if(is_array($products))
//        {
//            foreach ($products as $key => &$product)
//            {
//                if(!empty($product['mnumi_id']))
//                {
//                    $product = $mnumi->getMnumiCalculation($product);
//                    if(empty($product))
//                    {
//                        unset($products[$key]);
//                    }
//                }
//            }
//        }
        $this->cat_products = $products;
        Hook::exec('actionProductListModifier', array(
            'nb_products' => &$this->nbProducts,
            'cat_products' => &$this->cat_products,
        ));

        foreach ($this->cat_products as &$product)
        {
            if ($product['id_product_attribute'] && isset($product['product_attribute_minimal_quantity']))
            {
            $product['minimal_quantity'] = $product['product_attribute_minimal_quantity'];
            }
        }

        $this->addColorsToProductList($this->cat_products);

        $this->context->smarty->assign('nb_products', $this->nbProducts);
    }
}
