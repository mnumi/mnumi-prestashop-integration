# PrestaShop Mnumi Integration Module #

# Installation #
1. Upload files but keep the directory structure.
2. Install module on Tab "Modules"
3. You must change in your theme file js/cart-summary.js
    1. Find function "deleteProductFromSummary"
    2. Replace "productAttributeId = parseInt(ids[1]);" to "productAttributeId = ids[1];"
4. You must replace your theme file "shopping-cart-product-line.tpl"
    1. Find line: *<a rel="nofollow" title="{l s='Delete'}" class="cart_quantity_delete" id="{$product.id_product}_{$product.id_product_attribute}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}" href="{$link->getPageLink('cart', true, NULL, "delete=1&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_address_delivery={$product.id_address_delivery|intval}&amp;token={$token_cart}")|escape:'html':'UTF-8'}"><i class="icon-trash"></i></a>*

    2. Replace to: *<a rel="nofollow" title="{l s='Delete'}" class="cart_quantity_delete" id="{$product.id_product}_{if $product.raw_attributes}{$product.raw_attributes|md5}{else}{$product.id_product_attribute|intval}{/if}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}" href="{$link->getPageLink('cart', true, NULL, "delete=1&amp;id_product={$product.id_product|intval}&amp;ipa={if $product.raw_attributes}{$product.raw_attributes|md5}{else}{$product.id_product_attribute|intval}{/if}&amp;id_address_delivery={$product.id_address_delivery|intval}&amp;token={$token_cart}")|escape:'html':'UTF-8'}"><i class="icon-trash"></i></a>*

    3. Find line:
*<tr id="product_{$product.id_product}_{$product.id_product_attribute}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}{if !empty($product.gift)}_gift{/if}" class="cart_item{if isset($productLast) && $productLast && (!isset($ignoreProductLast) || !$ignoreProductLast)} last_item{/if}{if isset($productFirst) && $productFirst} first_item{/if}{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0} alternate_item{/if} address_{$product.id_address_delivery|intval} {if $odd}odd{else}even{/if}">*

    4. Replace to:
*<tr id="product_{$product.id_product}_{if $product.raw_attributes}{$product.raw_attributes|md5}{else}{$product.id_product_attribute|intval}{/if}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}{if !empty($product.gift)}_gift{/if}" class="cart_item{if isset($productLast) && $productLast && (!isset($ignoreProductLast) || !$ignoreProductLast)} last_item{/if}{if isset($productFirst) && $productFirst} first_item{/if}{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0} alternate_item{/if} address_{$product.id_address_delivery|intval} {if $odd}odd{else}even{/if}">*



## Configuration ##
1. Add key and API url.
    1. Find module on tab "Modules"
    2. Press "Configure" button.
    3. Save or Save and check connection. If all is ok You can connect product with MnumiPanel.

## Warning ##
1. In the directory "theme" is the default theme "product.tpl" with added Hook.
2. You can add manually two hooks to Your theme to "product.tpl"
```
#!php
{hook h='mnumiWizard'}
```
```
#!php
{hook h='mnumiParams'}
```

2. After the conjunction of the product from the store with the product of Mnumi, all the combinations will be deleted.
3. Price of the product will depend on the calculation of Mnumi.
4. The new parameters will be taken from Mnumi.